/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.bean.Slider;
import com.localbuddies.dao.SliderDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */

@Repository(value = "SliderDAO")
@Transactional
public class SliderDAOIMPL extends ConnectionDao implements SliderDAO{

	@Override
	public List<Slider> list() throws Exception {
		List<Slider> sliderList = new ArrayList<Slider>();
		Slider slider = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT  * FROM slider ORDER BY seq_no ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				slider = new Slider();
				slider.setSliderId(rs.getLong("slider_id"));
				slider.setSeqNo(rs.getLong("seq_no"));
				slider.setAlterName(rs.getString("alter_name"));
				slider.setImage(rs.getString("image"));
				slider.setStatus(rs.getBoolean("status"));
				sliderList.add(slider);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return sliderList;
	}

}
