/**
 * 
 */
package com.localbuddies.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.bean.Feedback;
import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.FeedbackService;

/**
 * @author Ishwar Bathe
 * @since Oct 21, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/feedback")
public class FeedbackController {
	

	private final FeedbackService feedbackService;

	public FeedbackController(FeedbackService feedbackService) {
		super();
		this.feedbackService = feedbackService;
	}
	
	
	/****
	 * ADD FEEDBACK
	 **************/

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody Feedback feedback) {
		Response response = new Response();
		try {
			return feedbackService.addnew(feedback);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}


	
	/****
	 * FEEDBACK LIST
	 **************/

	@CrossOrigin
	@PostMapping("/list")
	public Response feedbackList(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return feedbackService.feedbackList(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	

}
