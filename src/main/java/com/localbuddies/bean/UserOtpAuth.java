/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/**
 * @author Ishwar
 * @since Oct 11, 2020
 * 
 */

@Entity
@Table(name = "user_otp_auth")
public class UserOtpAuth implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "otp_auth_id")
	private Long otpAuthId;
	
	@Column(name = "otp", length=255,nullable=false)
	private String otp;
	
	@Column(name = "uuid", unique = true, nullable=false, length=50)
	private String uuid;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;


	public Long getOtpAuthId() {
		return otpAuthId;
	}


	public void setOtpAuthId(Long otpAuthId) {
		this.otpAuthId = otpAuthId;
	}




	public String getOtp() {
		return otp;
	}


	public void setOtp(String otp) {
		this.otp = otp;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
	
	
	
	
}
