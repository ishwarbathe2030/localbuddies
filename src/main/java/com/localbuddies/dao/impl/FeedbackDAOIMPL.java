/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.bean.Feedback;
import com.localbuddies.constants.Filter;
import com.localbuddies.dao.FeedbackDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Oct 21, 2020
 * 
 */

@Repository(value = "FeedbackDAO")
@Transactional
public class FeedbackDAOIMPL extends ConnectionDao implements  FeedbackDAO{
	



	@Override
	public List<Feedback> feedbackList(Filter filter) throws Exception {
		List<Feedback> feedbackList = new ArrayList<Feedback>();
		Feedback feedback = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM feedback ";
			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				feedback = new Feedback();
				feedback.setFeedbackId(rs.getLong("feedback_id"));
				feedback.setCreateDate(rs.getDate("create_date"));
				feedback.setEmail(rs.getString("email"));
				feedback.setMessage(rs.getString("message"));
				feedback.setMobile(rs.getString("mobile"));
				feedback.setName(rs.getString("name"));
				feedback.setRating(rs.getInt("rating"));
				feedback.setReplyed(rs.getInt("replyed"));
				feedback.setReview(rs.getString("review"));
				feedback.setShowToSystem(rs.getInt("show_to_system"));
				feedback.setSubject(rs.getString("subject"));
				feedback.setType(rs.getInt("type"));
//				if(rs.getLong("user_id") > 0){
//					User user = repositoryDao.findById(User.class, rs.getLong("user_id"));
//					feedback.setUser(user);
//				}
				
				feedbackList.add(feedback);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return feedbackList;
	}

}
