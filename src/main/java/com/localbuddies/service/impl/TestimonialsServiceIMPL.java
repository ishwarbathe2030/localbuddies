/**
 * 
 */
package com.localbuddies.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.localbuddies.bean.Testimonials;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.TestimonialsDAO;
import com.localbuddies.service.TestimonialsService;
import com.localbuddies.setting.RepositoryDao;
import com.localbuddies.util.SaveFile;

/**
 * @author Ishwar Bathe
 * @since Nov 29, 2020
 * 
 */

@Service
public class TestimonialsServiceIMPL implements TestimonialsService {

	private final RepositoryDao repositoryDao;

	private final TestimonialsDAO testimonialsDAO;

	private final SaveFile saveFile;

	public TestimonialsServiceIMPL(RepositoryDao repositoryDao, TestimonialsDAO testimonialsDAO, SaveFile saveFile) {
		super();
		this.repositoryDao = repositoryDao;
		this.testimonialsDAO = testimonialsDAO;
		this.saveFile = saveFile;
	}

	@Override
	public Response addnew(Testimonials testimonials) throws Exception {
		Response response = new Response();
		try {
			if (null != testimonials.getBase64() && !testimonials.getBase64().isEmpty()) {
				testimonials.setImage(saveFile.saveBase64(testimonials.getBase64(), testimonials.getImageName()));
			}
			List<Testimonials> testimonialsList = testimonialsDAO.list();
			if (null != testimonialsList && testimonialsList.size() > 0) {
				Testimonials testimonial = testimonialsList.get(0);
				Long imageSeq = testimonial.getSeqNo() + 1;
				testimonials.setSeqNo(imageSeq);
			} else {
				testimonials.setSeqNo(1l);
			}
			repositoryDao.addnew(testimonials);
			response.setTitle(StatusConstance.SUCCESS_ADD);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_ADDED);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(Testimonials testimonials) throws Exception {
		Response response = new Response();
		try {
			Testimonials test = repositoryDao.findById(Testimonials.class, testimonials.getTestimonialId());
			if (test != null) {
				if (null != testimonials.getBase64() && !testimonials.getBase64().isEmpty()) {
					test.setImage(saveFile.saveBase64(testimonials.getBase64(), testimonials.getImageName()));
				}
				test.setDesignation(testimonials.getDesignation());
				test.setMessage(testimonials.getMessage());
				test.setName(testimonials.getName());
				test.setStatus(testimonials.isStatus());
				repositoryDao.update(test);
				response.setTitle(StatusConstance.SUCCESS_UPDATE);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.RECORD_UPDATED);
			}

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long testimonialId) throws Exception {
		Response response = new Response();
		try {
			Testimonials testimonial = repositoryDao.findById(Testimonials.class, testimonialId);
			if (null != testimonial) {
				repositoryDao.deleteObj(testimonial);
				response.setTitle(StatusConstance.RECORD_DELETED);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.RECORD_DELETED_MSG);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changeStatus(Long testimonialId) throws Exception {
		Response response = new Response();
		try {
			Testimonials testimonial = repositoryDao.findById(Testimonials.class, testimonialId);
			if (null != testimonial) {
				if (testimonial.isStatus())
					testimonial.setStatus(false);
				else
					testimonial.setStatus(true);
				repositoryDao.update(testimonial);
				response.setTitle("Testimonial  STATUS CHANGED");
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.STATUS_CHANGE_MSG + testimonial.isStatus());
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response list() throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(testimonialsDAO.list());
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response updatePosition(List<Testimonials> testimonials) throws Exception {
		Response response = new Response();
		List<Testimonials> testimonialList = new ArrayList<Testimonials>();
		try {
			if (null != testimonials && testimonials.size() > 0) {
				for (Testimonials test : testimonials) {
					Testimonials testomo = repositoryDao.findById(Testimonials.class, test.getTestimonialId());
					testomo.setSeqNo(test.getSeqNo());
					repositoryDao.update(testomo);
					testimonialList.add(testomo);
				}
			}
			response.setResponse(testimonialList);
			response.setTitle("NEW SEQUENCE SET");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Testimonials New Sequence Set Successfully");

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
