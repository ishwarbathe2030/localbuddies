/**
 * 
 */
package com.localbuddies.dao;


import com.localbuddies.cc.Dashboard;
import com.localbuddies.constants.Filter;

/**
 * @author Ishwar Bathe
 * @since Nov 12, 2020
 * 
 */
public interface DashboardDAO {

	
	public Dashboard dashboardAnylysis()throws Exception;
	
	public Dashboard orderAnylysisByStatus(Filter filter) throws Exception;
	
	public Dashboard dayWiseOrder(Filter filter) throws Exception;
	
	public Dashboard monthWiseOrder(Filter filter) throws Exception;
	
}
