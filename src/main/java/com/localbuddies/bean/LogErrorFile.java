/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Prashant Kolhe
 * @since Dec 26, 2020
 * 
 */

@Entity
@Table(name="log_error_file")
public class LogErrorFile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 827455274622708730L;

	
	@Id
	@Column(name = "log_error_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long logErrorFileId;
	
	@Column(name="log_text", columnDefinition="LONGTEXT")
	private String errorText;
	
	@Column(name="method_name")
	private String methodName;
	
	@Column(name ="error_class")
	private String errorClass;
	
	@Column(name="class_name")
	private String className;

	public Long getLogErrorFileId() {
		return logErrorFileId;
	}

	public void setLogErrorFileId(Long logErrorFileId) {
		this.logErrorFileId = logErrorFileId;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
}
