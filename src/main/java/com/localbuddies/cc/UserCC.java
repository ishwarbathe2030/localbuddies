/**
 * 
 */
package com.localbuddies.cc;

import java.util.Date;
import java.util.List;



/**
 * @author Prashant Kolhe
 * @since Oct 20, 2020
 * 
 */
public class UserCC {

	
	private Long userId;
	
	private String uuid;
	
	private String username;
	
	private String email;
	
	private String mobile;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;
	
	private Integer gender;
	
	private Date dob;
	
	private String profilePhoto;
	
	private Integer registerBy;
	
	private Integer registerWith;
	
	private Long socialUserId;
	
	private Date createDate;
	
	private Date lastOrderDate;
	
	
	private int totalOrders;
	
	private Double totalRevenue;
	
	

	List<AddressCC> address;

	
	
	/**
	 * @return the totalOrders
	 */
	public int getTotalOrders() {
		return totalOrders;
	}

	/**
	 * @param totalOrders the totalOrders to set
	 */
	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}
	
	
	
	
	/**
	 * @return the totalRevenue
	 */
	public Double getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * @param totalRevenue the totalRevenue to set
	 */
	public void setTotalRevenue(Double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the gender
	 */
	public Integer getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Integer gender) {
		this.gender = gender;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the profilePhoto
	 */
	public String getProfilePhoto() {
		return profilePhoto;
	}

	/**
	 * @param profilePhoto the profilePhoto to set
	 */
	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	/**
	 * @return the registerBy
	 */
	public Integer getRegisterBy() {
		return registerBy;
	}

	/**
	 * @param registerBy the registerBy to set
	 */
	public void setRegisterBy(Integer registerBy) {
		this.registerBy = registerBy;
	}

	/**
	 * @return the registerWith
	 */
	public Integer getRegisterWith() {
		return registerWith;
	}

	/**
	 * @param registerWith the registerWith to set
	 */
	public void setRegisterWith(Integer registerWith) {
		this.registerWith = registerWith;
	}

	/**
	 * @return the socialUserId
	 */
	public Long getSocialUserId() {
		return socialUserId;
	}

	/**
	 * @param socialUserId the socialUserId to set
	 */
	public void setSocialUserId(Long socialUserId) {
		this.socialUserId = socialUserId;
	}


	/**
	 * @return the address
	 */
	public List<AddressCC> getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(List<AddressCC> address) {
		this.address = address;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the lastOrderDate
	 */
	public Date getLastOrderDate() {
		return lastOrderDate;
	}

	/**
	 * @param lastOrderDate the lastOrderDate to set
	 */
	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}
	 
	 
	 
	 
	 
}
