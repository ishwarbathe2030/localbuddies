/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.bean.Slider;



/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */
public interface SliderDAO {

	public List<Slider> list()throws Exception;
}
