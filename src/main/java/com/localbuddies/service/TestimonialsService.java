/**
 * 
 */
package com.localbuddies.service;

import java.util.List;

import com.localbuddies.bean.Testimonials;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Nov 29, 2020
 * 
 */
public interface TestimonialsService {


	public Response addnew(Testimonials sliderImage) throws Exception;
	
	public Response update(Testimonials sliderImage) throws Exception;
	
	public Response delete(Long sliderImageId) throws Exception;

	public Response changeStatus(Long sliderImageId) throws Exception;

	public Response list() throws Exception;

	public Response updatePosition(List<Testimonials> sliderImages) throws Exception;
	
}
