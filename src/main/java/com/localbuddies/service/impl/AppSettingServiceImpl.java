/**
 * 
 */
package com.localbuddies.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.NotificationSetting;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.AppSettingDAO;
import com.localbuddies.service.AppSettingService;
import com.localbuddies.setting.RepositoryDao;

/**
 * @author ISHWAR BATHE
 * @since Nov 1, 2020
 * 
 */
@Service
public class AppSettingServiceImpl implements AppSettingService {

	private final RepositoryDao repositoryDao;

	private final AppSettingDAO appSettingDAO;

	public AppSettingServiceImpl(RepositoryDao repositoryDao, AppSettingDAO appSettingDAO) {
		super();
		this.repositoryDao = repositoryDao;
		this.appSettingDAO = appSettingDAO;
	}

	@Override
	public Response addnew(List<AppSetting> appSettingList) throws Exception {
		Response response = new Response();
		try {
			AppSetting setting = null;
			for (AppSetting appSetting : appSettingList) {
				if (appSetting.getAppSettingId() != null) {
					setting = repositoryDao.findById(AppSetting.class, appSetting.getAppSettingId());
					setting.setType(appSetting.getType());
					setting.setAppKey(appSetting.getAppKey());
					setting.setDescription(appSetting.getDescription());
					setting.setValue(appSetting.getValue());
					repositoryDao.update(setting);
				} else {
					repositoryDao.addnew(appSetting);
				}
			}
			response.setTitle(StatusConstance.SUCCESS_ADD);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_UPDATED);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(AppSetting appSetting) throws Exception {
		Response response = new Response();
		try {

			if (appSetting.getAppSettingId() != null) {
				AppSetting setting = repositoryDao.findById(AppSetting.class, appSetting.getAppSettingId());
				if (setting != null) {
					setting.setType(appSetting.getType());
					setting.setAppKey(appSetting.getAppKey());
					setting.setDescription(appSetting.getDescription());
					setting.setValue(appSetting.getValue());
					repositoryDao.update(setting);
					response.setTitle(StatusConstance.SUCCESS_UPDATE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.RECORD_UPDATED);
				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage(StatusConstance.RECORD_NOT_FOUNDED);
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("App Setting" + StatusConstance.ID_REQ_TO_UPDATE);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long id) throws Exception {
		Response response = new Response();
		try {
			if (id != null) {
				AppSetting setting = repositoryDao.findById(AppSetting.class, id);
				if (setting != null) {
					repositoryDao.deleteObj(setting);
					response.setTitle(StatusConstance.DELETE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.DELETE_MSG);

				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage(StatusConstance.RECORD_NOT_FOUNDED);
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Setting" + StatusConstance.ID_REQ_TO_UPDATE);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response deleteNotificationSetting(Long id) throws Exception {
		Response response = new Response();
		try {
			if (id != null) {
				NotificationSetting setting = repositoryDao.findById(NotificationSetting.class, id);
				if (setting != null) {
					repositoryDao.deleteObj(setting);
					response.setTitle(StatusConstance.DELETE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.DELETE_MSG);

				} else {
					response.setTitle(StatusConstance.DATA_NOT_FOUND);
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage(StatusConstance.RECORD_NOT_FOUNDED);
				}
			} else {
				response.setTitle(StatusConstance.ID_REQ);
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("Notification Setting" + StatusConstance.ID_REQ_TO_UPDATE);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response list(String type) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(appSettingDAO.list(type));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response updateNotificationList(List<NotificationSetting> notificationSettingList) throws Exception {
		Response response = new Response();
		try {
			NotificationSetting notif = null;
			for (NotificationSetting nofificationSetting : notificationSettingList) {
				if (nofificationSetting.getNotificationSettingId() != null
						&& nofificationSetting.getNotificationSettingId() > 0) {
					notif = repositoryDao.findById(NotificationSetting.class,
							nofificationSetting.getNotificationSettingId());
					notif.setEmail(nofificationSetting.getEmail());
					notif.setMobile(nofificationSetting.getMobile());
					notif.setSendEmail(nofificationSetting.isSendEmail());
					notif.setSendFeedbackNotification(nofificationSetting.isSendFeedbackNotification());
					notif.setSendSMS(nofificationSetting.isSendSMS());
					notif.setSendOrderNotification(nofificationSetting.isSendOrderNotification());
					notif.setUserName(nofificationSetting.getUserName());
					repositoryDao.update(notif);
				} else {
					repositoryDao.addnew(nofificationSetting);
				}
			}
			response.setTitle(StatusConstance.SUCCESS_UPDATE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_UPDATED);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response notificationSettingList() throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(appSettingDAO.notificationSettingList());
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
