/**
 * 
 */
package com.localbuddies.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.localbuddies.bean.User;
import com.localbuddies.setting.RepositoryDao;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Ishwar Bathe
 * @since Nov 21, 2020
 * 
 */
@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

	private final RepositoryDao repositoryDao;

	public RequestInterceptor(RepositoryDao repositoryDao) {
		super();
		this.repositoryDao = repositoryDao;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
				return true;
				
			
//		System.out.println("URL = "+request.getRequestURI());
//	
//		response.setHeader("Access-Control-Allow-Credentials", "true");
//		response.setHeader("Access-Control-Allow-Origin", "*");
//		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
//		response.setHeader("Access-Control-Max-Age", "3600");
//		response.setHeader("Access-Control-Allow-Headers",
//				"Origin, X-Requested-With, Bearer, Content-Type, Accept, Authorization, timezone");
//
//		if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
//			return true;
//		} else if (
//				request.getRequestURI().contains("/v1/authentication/sign-up")
//				|| request.getRequestURI().contains("/v1/authentication/login-with-password")
//				|| request.getRequestURI().contains("/v1/authentication/login-with-otp")
//				|| request.getRequestURI().contains("/v1/authentication/change-password")
//				|| request.getRequestURI().contains("/v1/authentication/update-profile")
//				|| request.getRequestURI().contains("/v1/authentication/find-user-account")
//				|| request.getRequestURI().contains("/v1/authentication/send-otp")
//				|| request.getRequestURI().contains("/v1/authentication/verify-otp")
//				|| request.getRequestURI().contains("/v1/authentication/set-new-password")
//				|| request.getRequestURI().contains("/v1/file/")
//				|| request.getRequestURI().contains("/v1/store/list")
//				|| request.getRequestURI().contains("/v1/product/web/product-list")
//				|| request.getRequestURI().contains("/v1/category/web/category-with-subcategory")
//				|| request.getRequestURI().contains("/v1/pramotional/web/pramotional-list")
//				|| request.getRequestURI().contains("/v1/favourite/web/favourite-products")
//				|| request.getRequestURI().contains("/v1/slider/list")
//				|| request.getRequestURI().contains("/v1/slider/list")
//				|| request.getRequestURI().contains("/v1/product/web/product-list")
//				|| request.getRequestURI().contains("/v1/product/product-with-related-prod/")
//				|| request.getRequestURI().contains("/v1/product/web/product-by-category")
//				|| request.getRequestURI().contains("/assets/img/")
//				|| request.getRequestURI().contains("https://agrosetu.in/")
//				
//				
//				request.getRequestURI().contains("/Agrosetu/v1/authentication/sign-up")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/login-with-password")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/login-with-otp")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/change-password")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/update-profile")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/find-user-account")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/send-otp")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/verify-otp")
//				|| request.getRequestURI().contains("/Agrosetu/v1/authentication/set-new-password")
//				|| request.getRequestURI().contains("/Agrosetu/v1/file/")
//				|| request.getRequestURI().contains("/Agrosetu/v1/store/list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/product/web/product-list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/category/web/category-with-subcategory")
//				|| request.getRequestURI().contains("/Agrosetu/v1/pramotional/web/pramotional-list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/favourite/web/favourite-products")
//				|| request.getRequestURI().contains("/Agrosetu/v1/slider/list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/slider/list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/product/web/product-list")
//				|| request.getRequestURI().contains("/Agrosetu/v1/product/product-with-related-prod/")
//				|| request.getRequestURI().contains("/Agrosetu/v1/product/web/product-by-category")
//				|| request.getRequestURI().contains("/Agrosetu/assets/img/")
//				|| request.getRequestURI().contains("https://agrosetu.in/")
//				
//		) {
//			return super.preHandle(request, response, handler);
//		} else {
//
//			System.out.println("IN");
//			String securityAccessToken = request.getHeader("Bearer");
//			byte[] decoded = Base64.getDecoder().decode(securityAccessToken);
//			String[] satArr = new String(decoded).split(":");
//			String accessToken = satArr[0];
//			String uuid = satArr[2];
//			User user = repositoryDao.findBySignKey(User.class, "uuid", uuid);
//			if (user != null && user.getUuid().equals(uuid) && user.getAccessToken().equals(accessToken)) {
//				return super.preHandle(request, response, handler);
//			} else {
//				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//				return false;
//			}
//		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, //
			Object handler, ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, //
			Object handler, Exception ex) throws Exception {
	}

}
