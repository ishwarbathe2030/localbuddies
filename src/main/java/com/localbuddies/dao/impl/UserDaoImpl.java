/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.bean.UserLogger;
import com.localbuddies.cc.UserCC;
import com.localbuddies.constants.Filter;
import com.localbuddies.dao.AddressDAO;
import com.localbuddies.dao.UserDao;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */
@Repository(value = "UserDao")
@Transactional
public class UserDaoImpl extends ConnectionDao implements UserDao {

	private final AddressDAO addressDAO;

	public UserDaoImpl(AddressDAO addressDAO) {
		super();
		this.addressDAO = addressDAO;
	}

	@Override
	public List<UserCC> adminUserList(Filter filter) throws Exception {
		List<UserCC> userList = new ArrayList<UserCC>();
		Connection conn = null;
		UserCC user = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM user u WHERE u.user_type = 2 ";
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				sql += " AND u.first_name LIKE  ? OR  u.email LIKE ? OR u.mobile LIKE ? OR u.mobile LIKE ? OR u.last_name LIKE ? ";
			}
			sql += " ORDER BY " + filter.getOrderBy();
			if (filter.getPage() > 0 && filter.getSize() > 0) {
				sql += "  LIMIT  ?,? ";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			int count = 0;
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
			}
			if (filter.getPage() > 0 && filter.getSize() > 0) {
				ps.setLong(++count, (filter.getPage() - 1) * filter.getSize());
				ps.setLong(++count, filter.getSize());
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserCC();
				user.setUserId(rs.getLong("user_id"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getInt("gender"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setMiddleName(rs.getString("middle_name"));
				user.setMobile(rs.getString("mobile"));
				user.setProfilePhoto(rs.getString("profile_photo"));
				user.setRegisterBy(rs.getInt("register_by"));
				user.setRegisterWith(rs.getInt("register_with"));
				user.setUuid(rs.getString("uuid"));
				user.setCreateDate(rs.getTimestamp("create_date"));
				user.setSocialUserId(rs.getLong("social_user"));
				user.setAddress(addressDAO.userAddressList(user.getUserId()));
				userList.add(user);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userList;
	}

	@Override
	public Long userListCount(Filter filter) throws Exception {
		Long totalCount = 0L;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT COUNT(*) as totalCount FROM user u WHERE u.user_type = 2 ";
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				sql += " AND u.first_name LIKE  ? OR  u.email LIKE ? OR u.mobile LIKE ? OR u.mobile LIKE ? OR u.last_name LIKE ? ";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			int count = 0;
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
			}
			ResultSet rs = ps.executeQuery();
			System.out.println();
			while (rs.next()) {
				totalCount = rs.getLong("totalCount");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return totalCount;
	}

	@Override
	public List<UserCC> userReports(Filter filter) throws Exception {
		List<UserCC> userList = new ArrayList<UserCC>();
		Connection conn = null;
		UserCC user = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM user u WHERE u.user_type = 2 ";

			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(u.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(u.create_date) <= DATE('" + filter.getToDate() + "') ";

			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				sql += " AND u.first_name LIKE  ? OR  u.email LIKE ? OR u.mobile LIKE ? OR u.mobile LIKE ? OR u.last_name LIKE ? ";
			}
			sql += " ORDER BY " + filter.getOrderBy();
			if (filter.getPage() > 0 && filter.getSize() > 0) {
				sql += "  LIMIT  ?,? ";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			int count = 0;
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
			}
			if (filter.getPage() > 0 && filter.getSize() > 0) {
				ps.setLong(++count, (filter.getPage() - 1) * filter.getSize());
				ps.setLong(++count, filter.getSize());
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserCC();
				user.setUserId(rs.getLong("user_id"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getInt("gender"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setMiddleName(rs.getString("middle_name"));
				user.setMobile(rs.getString("mobile"));
				user.setProfilePhoto(rs.getString("profile_photo"));
				user.setRegisterBy(rs.getInt("register_by"));
				user.setRegisterWith(rs.getInt("register_with"));
				user.setUuid(rs.getString("uuid"));
				user.setCreateDate(rs.getTimestamp("create_date"));
				user.setSocialUserId(rs.getLong("social_user"));
				userList.add(user);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userList;
	}

	@Override
	public Long userReportsListCount(Filter filter) throws Exception {
		Long totalCount = 0L;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT COUNT(*) as totalCount FROM user u WHERE u.user_type = 2 ";

			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(u.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(u.create_date) <= DATE('" + filter.getToDate() + "') ";

			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				sql += " AND u.first_name LIKE  ? OR  u.email LIKE ? OR u.mobile LIKE ? OR u.mobile LIKE ? OR u.last_name LIKE ? ";
			}
			if (filter.getPage() > 0 && filter.getSize() > 0) {
				sql += " ORDER BY  u.first_name";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			int count = 0;
			if (filter.getSearchBy() != null && !filter.getSearchBy().isEmpty()) {
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
				ps.setString(++count, "%" + filter.getSearchBy() + "%");
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				totalCount = rs.getLong("totalCount");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return totalCount;
	}

	@Override
	public List<UserLogger> loggedDevices(Long userId) throws Exception {
		List<UserLogger> userLoggedList = new ArrayList<UserLogger>();
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM user_logger u WHERE u.user_id = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			UserLogger logger = null;
			while (rs.next()) {
				logger = new UserLogger();
				logger.setLoggerId(rs.getLong("logger_id"));
				logger.setDeviceUuid(rs.getString("device_uuid"));
				logger.setFcm(rs.getString("fcm"));
				logger.setLatitudes(rs.getDouble("latitudes"));
				logger.setLoggedCity(rs.getString("logged_city"));
				logger.setLongitudes(rs.getDouble("longitudes"));
				logger.setManufacturer(rs.getString("manufacturer"));
				logger.setModel(rs.getString("model"));
				logger.setPlatform(rs.getString("platform"));
				logger.setLogged(rs.getBoolean("logged"));
				logger.setUniqueDeviceId(rs.getString("unique_device_id"));
				logger.setVersion(rs.getString("version"));
				userLoggedList.add(logger);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return userLoggedList;
	}

}
