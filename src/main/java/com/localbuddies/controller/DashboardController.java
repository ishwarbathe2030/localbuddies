/**
 * 
 */
package com.localbuddies.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.DashboardService;

/**
 * @author Ishwar Bathe
 * @since Nov 15, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/dashboard")
public class DashboardController {
	
	private final DashboardService dashboardService;


	public DashboardController(DashboardService dashboardService) {
		super();
		this.dashboardService = dashboardService;
	}
	
	
	
	/****
	 * ORDER ANYLYSIS
	 **************/
	@CrossOrigin
	@GetMapping("/order-analysis")
	public Response dashboardAnylysis() {
		Response response = new Response();
		try {
			return dashboardService.dashboardAnylysis();
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * ORDER STATUS ANYLYSIS
	 **************/
	@CrossOrigin
	@PostMapping("/order-status-analysis")
	public Response orderAnylysisByStatus(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return dashboardService.orderAnylysisByStatus(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * ORDER DAYWISE ANYLYSIS
	 **************/
	@CrossOrigin
	@PostMapping("/order-daywise-analysis")
	public Response dayWiseOrder(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return dashboardService.dayWiseOrder(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	

	/****
	 * ORDER MONTHWISE ANYLYSIS
	 **************/
	@CrossOrigin
	@PostMapping("/order-monthwise-analysis")
	public Response monthWiseOrder(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return dashboardService.monthWiseOrder(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	

}
