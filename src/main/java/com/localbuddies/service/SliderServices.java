/**
 * 
 */
package com.localbuddies.service;

import java.util.List;

import com.localbuddies.bean.Slider;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */
public interface SliderServices {

	public Response addnew(Slider sliderImage) throws Exception;

	public Response delete(Long sliderImageId) throws Exception;

	public Response changeStatus(Long sliderImageId) throws Exception;

	public Response list() throws Exception;

	public Response updatePosition(List<Slider> sliderImages) throws Exception;

}
