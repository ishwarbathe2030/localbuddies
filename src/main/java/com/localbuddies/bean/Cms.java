/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */

@Entity
@Table(name = "cms")
public class Cms implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cms_id")
	private Long cmsId;
	
	
	


}
