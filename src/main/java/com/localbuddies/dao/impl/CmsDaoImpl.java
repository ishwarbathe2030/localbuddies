/**
 * 
 */
package com.localbuddies.dao.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.dao.CmsDao;
import com.localbuddies.setting.ConnectionDao;


/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */
@Repository(value = "CmsDao")
@Transactional
public class CmsDaoImpl extends ConnectionDao implements CmsDao{

}
