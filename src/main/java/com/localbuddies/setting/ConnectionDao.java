/**
 * 
 */
package com.localbuddies.setting;

import java.io.Serializable;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */
public class ConnectionDao {

	@Autowired
	private  SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	Transaction transaction=null;


	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void persist(Object entity) {
		getSession().persist(entity);
	}

	public void delete(Object entity) {
		getSession().delete(entity);
	}

	public <T> T getObjectById(Class<T> entity, Serializable id) {
		return (T) getSession().get(entity, id);
	}

	public void saveOrUpdate(Object entity) {
		getSession().saveOrUpdate(entity);
	}
	
	protected DataSource getDataSource(){
		return dataSource;
	}
	
	public void save(Object object) {
		try {
				transaction=getSession().beginTransaction();
				Serializable id=getSession().save(object);
				System.out.println("ID:-"+id);
				if(!transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
					transaction.commit();
				}
		}catch (Exception e) {
			if(transaction!=null) {
				transaction.rollback();
			}
		}
		
	}
}
