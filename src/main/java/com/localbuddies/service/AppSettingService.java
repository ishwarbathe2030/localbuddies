/**
 * 
 */
package com.localbuddies.service;

import java.util.List;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.NotificationSetting;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Nov 1, 2020
 * 
 */
public interface AppSettingService {

	public Response addnew(List<AppSetting> appSetting) throws Exception;

	public Response update(AppSetting appSetting) throws Exception;
	
	public Response updateNotificationList(List<NotificationSetting> notificationSettingList) throws Exception;
	
	public Response delete(Long id) throws Exception;
	
	public Response deleteNotificationSetting(Long id) throws Exception;
	
	public Response list(String type) throws Exception;
	
	public Response notificationSettingList() throws Exception;
	
}
