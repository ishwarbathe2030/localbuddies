/**
 * 
 */
package com.localbuddies.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.NotificationSetting;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.AppSettingService;

/**
 * @author Ishwar Bathe
 * @since Nov 5, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/appsetting")
public class AppSettingController {
	
	
	private final AppSettingService appSettingService;

	
	public AppSettingController(AppSettingService appSettingService) {
		super();
		this.appSettingService = appSettingService;
	}
	
	
	
	/****
	 * ADD Address
	 **************/

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody List<AppSetting> appSetting) {
		Response response = new Response();
		try {
			return appSettingService.addnew(appSetting);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * Update Address
	 **************/

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody  AppSetting appSetting) {
		Response response = new Response();
		try {
			return appSettingService.update(appSetting);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	

	/****
	 * Update Address
	 **************/

	@CrossOrigin
	@PutMapping("/update_notification_setting")
	public Response updateNotificationSetting(@RequestBody  List<NotificationSetting> notificationSettingList) {
		Response response = new Response();
		try {
			return appSettingService.updateNotificationList(notificationSettingList);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * DELETE NOTIFICATION
	 **************/
	@CrossOrigin
	@DeleteMapping("/delete-notification/{notificationSettingId}")
	public Response delete(@PathVariable("notificationSettingId") Long notificationSettingId) {
		Response response = new Response();
		try {
			return appSettingService.deleteNotificationSetting(notificationSettingId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * GET NOTIFICATON SETTING LIST
	 **************/
	@CrossOrigin
	@GetMapping("/list")
	public Response list() {
		Response response = new Response();
		try {
			return appSettingService.notificationSettingList();
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * GET APP  SETTING LIST
	 **************/
	@CrossOrigin
	@GetMapping("/appsetting-list/{type}")
	public Response list(@PathVariable("type") String type) {
		Response response = new Response();
		try {
			return appSettingService.list(type);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	

}
