/**
 * 
 */
package com.localbuddies.dao.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.dao.NotificationDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

@Repository(value = "notificationDAO")
@Transactional
public class NotificationDAOIMPL extends ConnectionDao implements NotificationDAO {

}
