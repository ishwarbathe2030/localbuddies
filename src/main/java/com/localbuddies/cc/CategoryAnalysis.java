/**
 * 
 */
package com.localbuddies.cc;

/**
 * @author Ishwar Bathe
 * @since Aug 4, 2020
 * 
 */
public class CategoryAnalysis {

	
	private Long totalCategory;
	
	private Long totalMainCategory;
	
	private Long totalActiveMainCategory;
	
	private Long totalInactiveMainCategory;
	
	private Long totalSubCategory;
	
	private Long totalActiveSubCategory;
	
	private Long totalInactiveSubCategory;

	public Long getTotalCategory() {
		return totalCategory;
	}

	public void setTotalCategory(Long totalCategory) {
		this.totalCategory = totalCategory;
	}

	public Long getTotalMainCategory() {
		return totalMainCategory;
	}

	public void setTotalMainCategory(Long totalMainCategory) {
		this.totalMainCategory = totalMainCategory;
	}

	public Long getTotalActiveMainCategory() {
		return totalActiveMainCategory;
	}

	public void setTotalActiveMainCategory(Long totalActiveMainCategory) {
		this.totalActiveMainCategory = totalActiveMainCategory;
	}

	public Long getTotalInactiveMainCategory() {
		return totalInactiveMainCategory;
	}

	public void setTotalInactiveMainCategory(Long totalInactiveMainCategory) {
		this.totalInactiveMainCategory = totalInactiveMainCategory;
	}

	public Long getTotalSubCategory() {
		return totalSubCategory;
	}

	public void setTotalSubCategory(Long totalSubCategory) {
		this.totalSubCategory = totalSubCategory;
	}

	public Long getTotalActiveSubCategory() {
		return totalActiveSubCategory;
	}

	public void setTotalActiveSubCategory(Long totalActiveSubCategory) {
		this.totalActiveSubCategory = totalActiveSubCategory;
	}

	public Long getTotalInactiveSubCategory() {
		return totalInactiveSubCategory;
	}

	public void setTotalInactiveSubCategory(Long totalInactiveSubCategory) {
		this.totalInactiveSubCategory = totalInactiveSubCategory;
	}

	
}
