/**
 * 
 */
package com.localbuddies.controller;



import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.cc.AddressCC;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.AddressService;


/**
 * @author Ishwar Bathe
 * @since Aug 28, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/address")
public class AddressController {

	private final AddressService addressService;

	public AddressController(AddressService addressService) {
		super();
		this.addressService = addressService;
	}

	/****
	 * ADD Address
	 **************/

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody AddressCC address) {
		Response response = new Response();
		try {
			return addressService.addnew(address);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	

	/****
	 * Update Address
	 **************/

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody AddressCC address) {
		Response response = new Response();
		try {
			return addressService.update(address);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * GET ADDRESS LIST BY USER ID
	 **************/
	@CrossOrigin
	@GetMapping("/list/{userId}")
	public Response userAddressList(@PathVariable("userId") Long userId) {
		Response response = new Response();
		try {
			return addressService.userAddressList(userId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * GET ADDRESS DETAIL
	 **************/
	@CrossOrigin
	@GetMapping("/{userAddressId}")
	public Response findById(@PathVariable("userAddressId") Long userAddressId) {
		Response response = new Response();
		try {
			return addressService.findById(userAddressId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * GET SELECTED ADDRESS
	 **************/
	@CrossOrigin
	@GetMapping("/selected-address/{userId}")
	public Response findSelectedAddress(@PathVariable("userId") Long userId) {
		Response response = new Response();
		try {
			return addressService.findSelectedAddress(userId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * SELECTED ADDRESS
	 **************/
	@CrossOrigin
	@GetMapping("/select-address/{userId}/{userAddressId}")
	public Response findById(@PathVariable("userId") Long userId, @PathVariable("userAddressId") Long userAddressId) {
		Response response = new Response();
		try {
			return addressService.changeSelected(userId, userAddressId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * DELETE ADDRESS
	 **************/
	@CrossOrigin
	@GetMapping("/delete-address/{userAddressId}")
	public Response delete(@PathVariable("userAddressId") Long userAddressId) {
		Response response = new Response();
		try {
			return addressService.delete(userAddressId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

}
