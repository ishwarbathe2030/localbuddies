/**
 * 
 */
package com.localbuddies.bean;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Ishwar
 * @since Oct 20, 2020
 * 
 */


@Entity
@Table(name = "feedback")
public class Feedback implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "feedback_id")
	private Long feedbackId;
	
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "subject")
	private String subject;
	
	@Column(name = "message", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String message;
	
	
	@Column(name = "review", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String review;
	
	@Column(name = "feeback_type")
	private String feedbackType;
	
	/***
	 1 For CONTACT 
	 2 For FEEDBACK
	 *********/
	@Column(name = "type", nullable=true, columnDefinition = "INTEGER DEFAULT 1")
	private Integer type;
	
	
	@Column(name = "rating", nullable=true, columnDefinition = "INTEGER DEFAULT 0")
	private Integer rating;
	
	
	/***
	 0 For No replyed 
	 1 For Replayed
	 *********/
	@Column(name = "replyed", nullable=true, columnDefinition = "INTEGER DEFAULT 0")
	private Integer replyed;
	
	
	
	/***
	 0 For Not show to other user
	 1 For Show to other user
	 *********/
	@Column(name = "show_to_system", nullable=true, columnDefinition = "INTEGER DEFAULT 0")
	private Integer showToSystem;
	
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modify_date")
	private Date modifyDate;
	
	
	@OneToOne
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	@Transient 
	private Long userId;

	/**
	 * @return the feedbackId
	 */
	public Long getFeedbackId() {
		return feedbackId;
	}


	/**
	 * @param feedbackId the feedbackId to set
	 */
	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}


	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}


	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the review
	 */
	public String getReview() {
		return review;
	}


	/**
	 * @param review the review to set
	 */
	public void setReview(String review) {
		this.review = review;
	}


	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}


	/**
	 * @return the rating
	 */
	public Integer getRating() {
		return rating;
	}


	/**
	 * @param rating the rating to set
	 */
	public void setRating(Integer rating) {
		this.rating = rating;
	}


	/**
	 * @return the replyed
	 */
	public Integer getReplyed() {
		return replyed;
	}


	/**
	 * @param replyed the replyed to set
	 */
	public void setReplyed(Integer replyed) {
		this.replyed = replyed;
	}


	/**
	 * @return the showToSystem
	 */
	public Integer getShowToSystem() {
		return showToSystem;
	}


	/**
	 * @param showToSystem the showToSystem to set
	 */
	public void setShowToSystem(Integer showToSystem) {
		this.showToSystem = showToSystem;
	}


	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}


	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}


	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}


	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}


	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}


	/**
	 * @return the feedbackType
	 */
	public String getFeedbackType() {
		return feedbackType;
	}


	/**
	 * @param feedbackType the feedbackType to set
	 */
	public void setFeedbackType(String feedbackType) {
		this.feedbackType = feedbackType;
	}
	
	
	
	
	
	
	


}
