/**
 * 
 */
package com.localbuddies.service;


import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Nov 12, 2020
 * 
 */
public interface DashboardService {

	Response dashboardAnylysis() throws Exception;
	
	public Response orderAnylysisByStatus(Filter filter) throws Exception;
	
	public Response dayWiseOrder(Filter filter) throws Exception;

	public Response monthWiseOrder(Filter filter) throws Exception;

}
