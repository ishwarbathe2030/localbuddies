/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.bean.UserLogger;
import com.localbuddies.cc.UserCC;
import com.localbuddies.constants.Filter;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */
public interface UserDao {

	List<UserCC> adminUserList(Filter filter) throws Exception;

	Long userListCount(Filter filter) throws Exception;

	List<UserCC> userReports(Filter filter) throws Exception;

	Long userReportsListCount(Filter filter) throws Exception;

	List<UserLogger> loggedDevices(Long userId) throws Exception;

}
