/**
 * 
 */
package com.localbuddies.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ishwar Bathe
 * @since Aug 2, 2020
 * 
 */

@RestController
@RequestMapping("/v1/file")
public class FileController {

	private final Environment environment;

	public FileController(Environment environment) {
		super();
		this.environment = environment;
	}

	@CrossOrigin
	@GetMapping(value = "/{name}")
	public void getImageFromName(@PathVariable("name") String name, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			FileInputStream fileInputStream = null;
			String imagePath = environment.getRequiredProperty("directory.path") + name;
			if (!imagePath.equals(null) && !imagePath.equalsIgnoreCase("null") && !imagePath.isEmpty()) {
				File file = new File(imagePath);
				if (file.exists()) {
					byte[] bFile = new byte[(int) file.length()];
					fileInputStream = new FileInputStream(file);
					fileInputStream.read(bFile);
					fileInputStream.close();
					response.setContentType("image/jpeg");
					response.getOutputStream().write(bFile);
					response.getOutputStream().flush();
				}
			}

		} catch (ClientAbortException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@CrossOrigin
	@GetMapping(value = "/download/{fileName}")
	public void getPdfFile(@PathVariable("fileName") String fileName, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String pdfPath = null;
			pdfPath = environment.getRequiredProperty("directory.filepath") + fileName;
			if (!pdfPath.equals(null) && !pdfPath.equalsIgnoreCase("null") && !pdfPath.isEmpty()) {
				File file = new File(pdfPath);
				if (file.exists()) {
					response.setContentType("application/octet-stream");
					response.setContentLength((int) file.length());

					// set headers for the response
					String headerKey = "Content-Disposition";
					String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
					response.setHeader(headerKey, headerValue);

					// get output stream of the response
					OutputStream outStream = response.getOutputStream();

					System.out.println(file);

					byte[] buffer = new byte[4096];
					int bytesRead = -1;
					FileInputStream inputStream = new FileInputStream(file);
					// write bytes read from the input stream into the output
					// stream
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outStream.write(buffer, 0, bytesRead);
					}
					inputStream.close();
					outStream.close();
				}
			}
		} catch (ClientAbortException ca) {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
