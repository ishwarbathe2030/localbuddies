/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.bean.Testimonials;
import com.localbuddies.dao.TestimonialsDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Nov 29, 2020
 * 
 */

@Repository(value = "testimonialsDAO")
@Transactional
public class TestimonialsDAOIMPL extends ConnectionDao implements TestimonialsDAO {

	@Override
	public List<Testimonials> list() throws Exception {
		List<Testimonials> testimonialsList = new ArrayList<Testimonials>();
		Testimonials testimonial = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT  * FROM testimonials ORDER BY seq_no ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				testimonial = new Testimonials();
				testimonial.setTestimonialId(rs.getLong("testimonial_id"));
				testimonial.setDesignation(rs.getString("designation"));
				testimonial.setMessage(rs.getString("message"));
				testimonial.setName(rs.getString("name"));
				testimonial.setSeqNo(rs.getLong("seq_no"));
				testimonial.setStatus(rs.getBoolean("status"));
				testimonial.setImage(rs.getString("image"));
				testimonialsList.add(testimonial);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return testimonialsList;
	}

}
