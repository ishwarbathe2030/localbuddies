/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Ishwar Bathe
 * @since Oct 12, 2020
 * 
 */

@Entity
@Table(name = "user_past_password")
public class UserPastPassword implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "past_pass_id")
	private Long pastPassId;
	
	
	@JsonIgnore
	@Column(name = "password_hash", nullable=true)
	private String passwordHash;
	
	
	@JsonIgnore
	@Column(name = "password_code", nullable=true)
	private String passwordCode;
	
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;
	
	
	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Long getPastPassId() {
		return pastPassId;
	}


	public void setPastPassId(Long pastPassId) {
		this.pastPassId = pastPassId;
	}


	public String getPasswordHash() {
		return passwordHash;
	}


	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}


	public String getPasswordCode() {
		return passwordCode;
	}


	public void setPasswordCode(String passwordCode) {
		this.passwordCode = passwordCode;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
	

}
