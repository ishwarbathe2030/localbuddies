/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.NotificationSetting;


/**
 * @author Prashant Kolhe
 * @since Nov 3, 2020
 * 
 */
public interface AppSettingDAO {
	
	public List<AppSetting> list(String type) throws Exception;
	
	public List<NotificationSetting>  notificationSettingList() throws Exception;
	
	public List<NotificationSetting>  notificationSettingListByFilter(NotificationSetting setting) throws Exception;
}
