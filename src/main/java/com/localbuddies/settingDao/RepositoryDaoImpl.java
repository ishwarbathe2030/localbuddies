/**
 * 
 */
package com.localbuddies.settingDao;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.localbuddies.setting.ConnectionDao;
import com.localbuddies.setting.RepositoryDao;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */

@Transactional
@Repository("RepositoryDao")
public class RepositoryDaoImpl extends ConnectionDao implements RepositoryDao {

	public void addnew(Object object) throws Exception {
		persist(object);
	};

	public void update(Object object) throws Exception {
		saveOrUpdate(object);
	};

	public void deleteObj(Object object) {
		delete(object);
	}

	@Override
	public <OBJ> OBJ findById(Class<OBJ> entity, Serializable id) {
		return (OBJ) getSession().get(entity, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <OBJ> OBJ findByKey(Class<OBJ> entity, String keyFirst, Object entityFirst, String keySecond,
			Object entitySecond) {
		Criteria criteria = getSession().createCriteria(entity).add(Restrictions.eq(keyFirst, entityFirst))
				.add(Restrictions.eq(keySecond, entitySecond));
		return (OBJ) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <OBJ> OBJ findBySingleKey(Class<OBJ> entity, String param, Object obj) {
		Criteria criteria = getSession().createCriteria(entity).add(Restrictions.eq(param, obj));
		return (OBJ) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <OBJ> OBJ list(Class<OBJ> entity, String keyFirst, String order, Object obj) throws Exception {
		Criteria criteria = getSession().createCriteria(entity).add(Restrictions.eq(keyFirst, obj))
				.addOrder(Order.desc(order));
		return (OBJ) criteria.list();
	}

	@Override
	public void addNewId(Object object) throws Exception {
		save(object);
	}

	@Override
	public String ErrorText(Exception e) throws Exception {
		StringWriter writer=new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		String[] lines=writer.toString().split("\n");
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<Math.min(lines.length, 100);i++) {
			sb.append(lines[i]).append("\n");
		}
		return sb.toString();
	}

}
