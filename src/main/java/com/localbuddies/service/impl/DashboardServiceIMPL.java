/**
 * 
 */
package com.localbuddies.service.impl;

import org.springframework.stereotype.Service;

import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.dao.DashboardDAO;
import com.localbuddies.service.DashboardService;

/**
 * @author Ishwar Bathe
 * @since Nov 12, 2020
 * 
 */

@Service
public class DashboardServiceIMPL implements DashboardService {

	private final DashboardDAO dashboardDAO;

	public DashboardServiceIMPL(DashboardDAO dashboardDAO) {
		super();
		this.dashboardDAO = dashboardDAO;
	}

	@Override
	public Response dashboardAnylysis() throws Exception {
		Response response = new Response();
		try {
			response.setStatus(200);
			response.setResponse(dashboardDAO.dashboardAnylysis());
			response.setMessage("Dashboard anylysis");
			response.setTitle("DASHBOARD ORDER ANYLYSIS");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response orderAnylysisByStatus(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setStatus(200);
			response.setResponse(dashboardDAO.orderAnylysisByStatus(filter));
			response.setMessage("Dashboard anylysis");
			response.setTitle("DASHBOARD ORDER ANYLYSIS");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response dayWiseOrder(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setStatus(200);
			response.setResponse(dashboardDAO.dayWiseOrder(filter));
			response.setMessage("Day Wise Anylysis");
			response.setTitle("DASHBOARD DAYWISE ANYLYSIS");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response monthWiseOrder(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setStatus(200);
			response.setResponse(dashboardDAO.monthWiseOrder(filter));
			response.setMessage("Month Wise Anylysis");
			response.setTitle("DASHBOARD MONTH ANYLYSIS");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
