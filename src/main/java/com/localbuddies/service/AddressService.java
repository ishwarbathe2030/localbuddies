/**
 * 
 */
package com.localbuddies.service;

import com.localbuddies.cc.AddressCC;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Aug 28, 2020
 * 
 */
public interface AddressService {

	public Response addnew(AddressCC address) throws Exception;

	public Response update(AddressCC address) throws Exception;

	public Response findById(Long userAddressId) throws Exception;
	
	public Response findSelectedAddress(Long userId) throws Exception;

	public Response delete(Long userAddressId) throws Exception;
	
	public Response changeSelected(Long userId ,Long userAddressId) throws Exception;
	
	public Response userAddressList(Long userId) throws Exception;
	
}
