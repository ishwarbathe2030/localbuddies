/**
 * 
 */
package com.localbuddies.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.localbuddies.bean.Address;
import com.localbuddies.bean.User;
import com.localbuddies.bean.UserAddress;
import com.localbuddies.cc.AddressCC;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.AddressDAO;
import com.localbuddies.service.AddressService;
import com.localbuddies.setting.RepositoryDao;

/**
 * @author Ishwar Bathe
 * @since Aug 28, 2020
 * 
 */

@Service
public class AddressServiceImpl implements AddressService {

	private final RepositoryDao repositoryDao;

	private final AddressDAO addressDAO;

	public AddressServiceImpl(RepositoryDao repositoryDao, AddressDAO addressDAO) {
		super();
		this.repositoryDao = repositoryDao;
		this.addressDAO = addressDAO;
	}

	@Override
	public Response addnew(AddressCC add) throws Exception {
		Response response = new Response();
		try {
			if (add.getUserId() != null) {
				User user = repositoryDao.findById(User.class, add.getUserId());
				if (user != null) {
					Address address = new Address();

					address.setArea(add.getArea());
					address.setCity(add.getCity());
					address.setCountry(add.getCountry());
					address.setFullAddress(add.getFullAddress());
					address.setLandmark(add.getLandmark());
					address.setLatitudes(add.getLatitudes());
					address.setLine1(add.getLine1());
					address.setLongitudes(add.getLongitudes());
					address.setPostalCode(add.getPostalCode());
					address.setState(add.getState());
					address.setLine2(add.getLine2());
					address.setType(add.getType());
					repositoryDao.addnew(address);

					UserAddress userAddress = new UserAddress();
					userAddress.setAddress(address);
					userAddress.setUser(user);

					List<AddressCC> addressList = addressDAO.userAddressList(add.getUserId());
					UserAddress uAddress = null;
					if (addressList.size() > 0) {
						for (AddressCC ccAdd : addressList) {
							if (ccAdd.getUserAddressId() != null) {
								uAddress = repositoryDao.findById(UserAddress.class, ccAdd.getUserAddressId());
								if (uAddress != null) {
									uAddress.setSelected(false);
									repositoryDao.update(uAddress);
								}
							}
						}
					}
					userAddress.setSelected(true);
					repositoryDao.addnew(userAddress);
					response.setTitle(StatusConstance.SUCCESS_ADD);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.RECORD_ADDED);
					response.setResponse(add);
				} else {
					response.setTitle("USER NOT FOUND. PASS VALID USER ID");
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("Something is wrong.");
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response update(AddressCC add) throws Exception {
		Response response = new Response();
		try {
			Address address = repositoryDao.findById(Address.class, add.getAddressId());
			if (address != null) {

				address.setArea(add.getArea());
				address.setCity(add.getCity());
				address.setCountry(add.getCountry());
				address.setFullAddress(add.getFullAddress());
				address.setLandmark(add.getLandmark());
				address.setLatitudes(add.getLatitudes());
				address.setLine1(add.getLine1());
				address.setLongitudes(add.getLongitudes());
				address.setPostalCode(add.getPostalCode());
				address.setState(add.getState());
				address.setLine2(add.getLine2());
				address.setType(add.getType());
				repositoryDao.update(address);

				response.setTitle(StatusConstance.SUCCESS_UPDATE);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.RECORD_UPDATED);
			} else {
				response.setTitle(StatusConstance.DATA_NOT_FOUND);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.RECORD_NOT_FOUND);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response findById(Long userAddressId) throws Exception {
		Response response = new Response();
		try {
			UserAddress userAddress = repositoryDao.findById(UserAddress.class, userAddressId);
			if (userAddress != null) {
				Address add = repositoryDao.findById(Address.class, userAddress.getAddress().getAddressId());
				if (add != null) {

					AddressCC address = new AddressCC();
					address.setArea(add.getArea());
					address.setCity(add.getCity());
					address.setCountry(add.getCountry());
					address.setFullAddress(add.getFullAddress());
					address.setLandmark(add.getLandmark());
					address.setLatitudes(add.getLatitudes());
					address.setLine1(add.getLine1());
					address.setLongitudes(add.getLongitudes());
					address.setPostalCode(add.getPostalCode());
					address.setState(add.getState());
					address.setLine2(add.getLine2());
					address.setType(add.getType());
					address.setUserAddressId(userAddress.getUserAddressId());
					address.setSelected(userAddress.isSelected());
					response.setResponse(address);

				}
			}
			response.setTitle(StatusConstance.DATA_FOUND);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_FOUND);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long userAddressId) throws Exception {
		Response response = new Response();
		try {
			UserAddress userAddress = repositoryDao.findById(UserAddress.class, userAddressId);
			Long addressId = userAddress.getAddress().getAddressId();
			if (userAddress != null) {
				repositoryDao.deleteObj(userAddress);
				Address address = repositoryDao.findById(Address.class, addressId);
				if (address != null) {
					repositoryDao.deleteObj(address);
				}
			}
			response.setTitle(StatusConstance.RECORD_DELETED);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_DELETED_MSG);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changeSelected(Long userId, Long userAddressId) throws Exception {
		Response response = new Response();
		try {

			List<AddressCC> addressList = addressDAO.userAddressList(userId);
			UserAddress uAddress = null;
			if (addressList.size() > 0) {
				for (AddressCC ccAdd : addressList) {
					if (ccAdd.getUserAddressId() != null) {
						uAddress = repositoryDao.findById(UserAddress.class, ccAdd.getUserAddressId());
						if (uAddress != null) {
							uAddress.setSelected(false);
							repositoryDao.update(uAddress);
						}
					}
				}
			}

			UserAddress userAddress = repositoryDao.findById(UserAddress.class, userAddressId);
			if (userAddress != null) {
				userAddress.setSelected(true);
				repositoryDao.update(userAddress);
			}
			response.setTitle(StatusConstance.DEFUALT_ADDRESS_CHANGED);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.DEFUALT_ADDRESS_CHANGED_TITLE);

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response userAddressList(Long userId) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(addressDAO.userAddressList(userId));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response findSelectedAddress(Long userId) throws Exception {
		Response response = new Response();
		try {
			List<AddressCC> addressList = addressDAO.userAddressList(userId);
			if (addressList.size() > 0) {
				for (AddressCC ccAdd : addressList) {
					if (ccAdd.isSelected()) {
						response.setResponse(ccAdd);
					}
				}
			}
			response.setTitle(StatusConstance.DATA_FOUND);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_FOUND);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
