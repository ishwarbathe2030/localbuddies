/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.NotificationSetting;
import com.localbuddies.dao.AppSettingDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Nov 3, 2020
 * 
 */

@Repository(value = "appSettingDAO")
@Transactional
public class AppSettingDAOIMPL extends ConnectionDao implements AppSettingDAO {

	@Override
	public List<AppSetting> list(String type) throws Exception {
		List<AppSetting> appSettingList = new ArrayList<AppSetting>();
		AppSetting setting = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM app_setting WHERE appkey = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				setting = new AppSetting();
				setting.setAppKey(rs.getString("appkey"));
				setting.setAppSettingId(rs.getLong("app_setting_id"));
				setting.setDescription(rs.getString("description"));
				setting.setType(rs.getString("type"));
				setting.setValue(rs.getString("value"));
				appSettingList.add(setting);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return appSettingList;
	}

	@Override
	public List<NotificationSetting> notificationSettingList() throws Exception {
		List<NotificationSetting> appSettingList = new ArrayList<NotificationSetting>();
		NotificationSetting setting = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM notification_setting";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				setting = new NotificationSetting();
				setting.setNotificationSettingId(rs.getLong("notification_setting_id"));
				setting.setEmail(rs.getString("email"));
				setting.setMobile(rs.getString("mobile"));
				setting.setSendEmail(rs.getBoolean("send_email"));
				setting.setSendFeedbackNotification(rs.getBoolean("send_feedback_notification"));
				setting.setSendSMS((rs.getBoolean("send_sms")));
				setting.setSendOrderNotification(rs.getBoolean("send_order_notification"));
				setting.setUserName(rs.getString("user_name"));
				appSettingList.add(setting);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return appSettingList;
	}

	@Override
	public List<NotificationSetting> notificationSettingListByFilter(NotificationSetting setting1) throws Exception {
		List<NotificationSetting> appSettingList = new ArrayList<NotificationSetting>();
		NotificationSetting setting = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM notification_setting ns WHERE ns.notification_setting_id IS NOT NULL ";
			if (setting1.getEmailNotificaion() != null && setting1.getEmailNotificaion().equals("emailNotification")
					&& !setting1.getEmailNotificaion().isEmpty()) {
				sql += " AND ns.send_email = 1";
			}
			if (setting1.getOrderNotification() != null && setting1.getOrderNotification().equals("orderNotitification")
					&& !setting1.getOrderNotification().isEmpty()) {
				sql += " AND ns.send_order_notification = 1";
			}

			if (setting1.getFeedbackNotification() != null
					&& setting1.getFeedbackNotification().equals("feedbackNotitification")
					&& !setting1.getFeedbackNotification().isEmpty()) {
				sql += " AND ns.send_feedback_notification = 1";
			}

			if (setting1.getSmsNotification() != null && setting1.getSmsNotification().equals("smsNotitification")
					&& !setting1.getSmsNotification().isEmpty()) {
				sql += "  AND ns.send_sms = 1 ";
			}

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				setting = new NotificationSetting();
				setting.setNotificationSettingId(rs.getLong("notification_setting_id"));
				setting.setEmail(rs.getString("email"));
				setting.setMobile(rs.getString("mobile"));
				setting.setSendEmail(rs.getBoolean("send_email"));
				setting.setSendFeedbackNotification(rs.getBoolean("send_feedback_notification"));
				setting.setSendSMS((rs.getBoolean("send_sms")));
				setting.setSendOrderNotification(rs.getBoolean("send_order_notification"));
				setting.setUserName(rs.getString("user_name"));
				appSettingList.add(setting);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return appSettingList;
	}

}
