/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.bean.Testimonials;



/**
 * @author Ishwar Bathe
 * @since Nov 29, 2020
 * 
 */
public interface TestimonialsDAO {

	public List<Testimonials> list() throws Exception;

}
