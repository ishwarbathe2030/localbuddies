/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.cc.AddressCC;

/**
 * @author Ishwar Bathe
 * @since Aug 28, 2020
 * 
 */
public interface AddressDAO {
	
	public List<AddressCC> userAddressList(Long userId) throws Exception;
	

}
