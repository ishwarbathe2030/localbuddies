/**
 * 
 */
package com.localbuddies.service;

import com.localbuddies.bean.Feedback;
import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Oct 21, 2020
 * 
 */
public interface FeedbackService {
	

	public Response addnew(Feedback feedback) throws Exception;
	
	public Response feedbackList(Filter filter)  throws Exception ;

}
