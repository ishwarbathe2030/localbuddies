/**
 * 
 */
package com.localbuddies.service.impl;

import org.springframework.stereotype.Service;

import com.localbuddies.dao.NotificationDAO;
import com.localbuddies.service.NotificationService;
import com.localbuddies.setting.RepositoryDao;

/**
 * @author ISHWAR BATHE
 * @since DECEMBER 13, 2020
 * 
 */

@Service
public class NotificationServiceIMPL implements  NotificationService{

	public final RepositoryDao repositoryDao;
	
	public final NotificationDAO notificationDAO;

	public NotificationServiceIMPL(RepositoryDao repositoryDao, NotificationDAO notificationDAO) {
		super();
		this.repositoryDao = repositoryDao;
		this.notificationDAO = notificationDAO;
	}
	
	
	
}
