/**
 * 
 */
package com.localbuddies.cc;

import java.util.List;

/**
 * @author Ishwar Bathe
 * @since Nov 12, 2020
 * 
 */
public class Dashboard {

	private int totalOrders;

	private int totalRevenue;

	private int todaysOrders;

	private int todaysRevenues;

	/**
	 * Following two use for order status Ex. delivered : 10 , Submitted: 50
	 */
	private List<String> orderStatus;

	private List<Integer> orderStatusCount;

	/**
	 * Following two use for month wise order Ex. March : 100 , April: 50
	 * 
	 */
	private List<String> months;

	private List<Integer> monthlyCount;
	
	
	private List<String> lable;

	private List<Integer> data;

	public int getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}

	public int getTodaysOrders() {
		return todaysOrders;
	}

	public void setTodaysOrders(int todaysOrders) {
		this.todaysOrders = todaysOrders;
	}


	public int getTodaysRevenues() {
		return todaysRevenues;
	}

	public void setTodaysRevenues(int todaysRevenues) {
		this.todaysRevenues = todaysRevenues;
	}

	public List<String> getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(List<String> orderStatus) {
		this.orderStatus = orderStatus;
	}

	public List<Integer> getOrderStatusCount() {
		return orderStatusCount;
	}

	public void setOrderStatusCount(List<Integer> orderStatusCount) {
		this.orderStatusCount = orderStatusCount;
	}

	public List<String> getMonths() {
		return months;
	}

	public void setMonths(List<String> months) {
		this.months = months;
	}

	public List<Integer> getMonthlyCount() {
		return monthlyCount;
	}

	public void setMonthlyCount(List<Integer> monthlyCount) {
		this.monthlyCount = monthlyCount;
	}

	public int getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(int totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the lable
	 */
	public List<String> getLable() {
		return lable;
	}

	/**
	 * @param lable the lable to set
	 */
	public void setLable(List<String> lable) {
		this.lable = lable;
	}

	public List<Integer> getData() {
		return data;
	}


	public void setData(List<Integer> data) {
		this.data = data;
	}
	
	
	
	

}
