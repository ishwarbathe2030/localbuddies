/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.localbuddies.cc.AddressCC;
import com.localbuddies.dao.AddressDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Aug 28, 2020
 * 
 */

@Repository(value = "addressDAO")
@Transactional
public class AddressDAOIMPL extends ConnectionDao implements AddressDAO {

	@Override
	public List<AddressCC> userAddressList(Long userId) throws Exception {
		List<AddressCC> addressList = new ArrayList<AddressCC>();
		AddressCC address = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = "SELECT * FROM address a INNER JOIN user_address ua ON a.address_id = ua.adddress_id WHERE  ua.user_id = ? ORDER BY ua.selected DESC";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				address = new AddressCC();
				address.setAddressId(rs.getLong("address_id"));
				address.setArea(rs.getString("area"));
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setFullAddress(rs.getString("full_address"));
				address.setLandmark(rs.getString("landmark"));
				address.setLatitudes(rs.getDouble("latitudes"));
				address.setLine1(rs.getString("line1"));
				address.setLongitudes(rs.getDouble("longitudes"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setState(rs.getString("state"));
				address.setLine2(rs.getString("line2"));
				address.setType(rs.getString("type"));
				address.setUserAddressId(rs.getLong("user_address_id"));
				address.setSelected(rs.getBoolean("selected"));
				address.setUserId(rs.getLong("user_id"));
				addressList.add(address);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return addressList;
	}

}
