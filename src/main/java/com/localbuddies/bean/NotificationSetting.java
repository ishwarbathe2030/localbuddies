/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Ishwar
 * @since Nov 5, 2020
 * 
 */

@Entity
@Table(name = "notification_setting")
public class NotificationSetting implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notification_setting_id")
	private Long notificationSettingId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "email")
	private String email;

	@Column(name = "send_email", columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean sendEmail;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "send_sms", columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean sendSMS;

	@Column(name = "send_order_notification", columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean sendOrderNotification;

	@Column(name = "send_feedback_notification", columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean sendFeedbackNotification;

	@Transient
	private String orderNotification;

	@Transient
	private String feedbackNotification;

	@Transient
	private String emailNotificaion;

	@Transient
	private String smsNotification;
	

	/**
	 * @return the notificationSettingId
	 */
	public Long getNotificationSettingId() {
		return notificationSettingId;
	}

	/**
	 * @param notificationSettingId
	 *            the notificationSettingId to set
	 */
	public void setNotificationSettingId(Long notificationSettingId) {
		this.notificationSettingId = notificationSettingId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the sendEmail
	 */
	public boolean isSendEmail() {
		return sendEmail;
	}

	/**
	 * @param sendEmail
	 *            the sendEmail to set
	 */
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the sendSMS
	 */
	public boolean isSendSMS() {
		return sendSMS;
	}

	/**
	 * @param sendSMS
	 *            the sendSMS to set
	 */
	public void setSendSMS(boolean sendSMS) {
		this.sendSMS = sendSMS;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sendOrderNotification
	 */
	public boolean isSendOrderNotification() {
		return sendOrderNotification;
	}

	/**
	 * @param sendOrderNotification
	 *            the sendOrderNotification to set
	 */
	public void setSendOrderNotification(boolean sendOrderNotification) {
		this.sendOrderNotification = sendOrderNotification;
	}

	/**
	 * @return the sendFeedbackNotification
	 */
	public boolean isSendFeedbackNotification() {
		return sendFeedbackNotification;
	}

	/**
	 * @param sendFeedbackNotification
	 *            the sendFeedbackNotification to set
	 */
	public void setSendFeedbackNotification(boolean sendFeedbackNotification) {
		this.sendFeedbackNotification = sendFeedbackNotification;
	}

	/**
	 * @return the orderNotification
	 */
	public String getOrderNotification() {
		return orderNotification;
	}

	/**
	 * @param orderNotification the orderNotification to set
	 */
	public void setOrderNotification(String orderNotification) {
		this.orderNotification = orderNotification;
	}

	/**
	 * @return the feedbackNotification
	 */
	public String getFeedbackNotification() {
		return feedbackNotification;
	}

	/**
	 * @param feedbackNotification the feedbackNotification to set
	 */
	public void setFeedbackNotification(String feedbackNotification) {
		this.feedbackNotification = feedbackNotification;
	}

	/**
	 * @return the emailNotificaion
	 */
	public String getEmailNotificaion() {
		return emailNotificaion;
	}

	/**
	 * @param emailNotificaion the emailNotificaion to set
	 */
	public void setEmailNotificaion(String emailNotificaion) {
		this.emailNotificaion = emailNotificaion;
	}

	/**
	 * @return the smsNotification
	 */
	public String getSmsNotification() {
		return smsNotification;
	}

	/**
	 * @param smsNotification the smsNotification to set
	 */
	public void setSmsNotification(String smsNotification) {
		this.smsNotification = smsNotification;
	}
	
	
	

}
