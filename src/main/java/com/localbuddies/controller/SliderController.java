/**
 * 
 */
package com.localbuddies.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.bean.Slider;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.SliderServices;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */
@RestController
@RequestMapping(value = "/v1/slider")
public class SliderController {
	
	private final SliderServices sliderService;
	

	public SliderController(SliderServices sliderService) {
		super();
		this.sliderService = sliderService;
	}



	/****
	 * ADD Slider
	 **************/

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody Slider slider) {
		Response response = new Response();
		try {
			return sliderService.addnew(slider);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * UPDATE POSITON OF SLIDER
	 **************/

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody List<Slider> slider) {
		Response response = new Response();
		try {
			return sliderService.updatePosition(slider);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * DELETE SLIDER
	 **************/
	@CrossOrigin
	@DeleteMapping("/delete/{sliderId}")
	public Response delete(@PathVariable("sliderId") Long sliderId) {
		Response response = new Response();
		try {
			return sliderService.delete(sliderId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * CHANGE STATUS OF  SLIDER
	 **************/
	@CrossOrigin
	@GetMapping("/change-status/{sliderId}")
	public Response changeStatus(@PathVariable("sliderId") Long sliderId) {
		Response response = new Response();
		try {
			return sliderService.changeStatus(sliderId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * CHANGE STATUS OF  SLIDER
	 **************/
	@CrossOrigin
	@GetMapping("/list")
	public Response list() {
		Response response = new Response();
		try {
			return sliderService.list();
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	

}
