/**
 * 
 */
package com.localbuddies.service.impl;

import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.localbuddies.bean.Feedback;
import com.localbuddies.bean.User;
import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.FeedbackDAO;
import com.localbuddies.service.FeedbackService;
import com.localbuddies.setting.RepositoryDao;
import com.localbuddies.util.SendMail;

/**
 * @author Ishwar Bathe
 * @since Oct 21, 2020
 * 
 */

@Service
public class FeedbackServiceIMPL implements FeedbackService {

	private final RepositoryDao repositoryDao;

	private final FeedbackDAO feedbackDAO;

	private final SendMail sendMail;

	public FeedbackServiceIMPL(RepositoryDao repositoryDao, FeedbackDAO feedbackDAO, SendMail sendMail) {
		super();
		this.repositoryDao = repositoryDao;
		this.feedbackDAO = feedbackDAO;
		this.sendMail = sendMail;
	}

	@Override
	public Response addnew(Feedback feedback) throws Exception {
		Response response = new Response();
		try {
			if (feedback.getUserId() != null) {
				User user = repositoryDao.findById(User.class, feedback.getUserId());
				feedback.setUser(user);
			}
			repositoryDao.addnew(feedback);
			if (feedback.getEmail() != null && !feedback.getEmail().isEmpty()) {
				sendOrderMail(feedback.getEmail());
			}
			response.setTitle("SUMITTED");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Request Sumitted");

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	public void sendOrderMail(String email) throws Exception {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setOrder(0);

		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);

		Context context = new Context();
		context.setVariable("order", email);

		String body = templateEngine.process("contactus.html", context);

		sendMail.sendEmail(email, "Thanks for contact to Agrosetu", body);

	}

	@Override
	public Response feedbackList(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(feedbackDAO.feedbackList(filter));
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
