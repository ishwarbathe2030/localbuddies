/**
 * 
 */
package com.localbuddies.service;


import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */
public interface UserService {
	
	
	Response adminUserList(Filter filter)throws Exception;
	
	Response userReports(Filter filter)throws Exception;

}
