/**
 * 
 */
package com.localbuddies.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.bean.Testimonials;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.TestimonialsService;

/**
 * @author Ishwar Bathe
 * @since Nov 29, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/testimonials")
public class TestimonialsController {

	private final TestimonialsService testimonialsService;

	public TestimonialsController(TestimonialsService testimonialsService) {
		super();
		this.testimonialsService = testimonialsService;
	}

	/****
	 * ADD TESTIMONIAL
	 **************/

	@CrossOrigin
	@PostMapping("/add")
	public Response addnew(@RequestBody Testimonials testimonials) {
		Response response = new Response();
		try {
			return testimonialsService.addnew(testimonials);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * UPDATE OF TESTIMONIAL
	 **************/

	@CrossOrigin
	@PutMapping("/update")
	public Response update(@RequestBody Testimonials testimonials) {
		Response response = new Response();
		try {
			return testimonialsService.update(testimonials);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * UPDATE POSITON OF TESTIMONIAL
	 **************/

	@CrossOrigin
	@PutMapping("/update-position")
	public Response TESTIMONIAL(@RequestBody List<Testimonials> testimonials) {
		Response response = new Response();
		try {
			return testimonialsService.updatePosition(testimonials);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * DELETE SLIDER
	 **************/
	@CrossOrigin
	@DeleteMapping("/delete/{testimonialsId}")
	public Response delete(@PathVariable("testimonialsId") Long testimonialsId) {
		Response response = new Response();
		try {
			return testimonialsService.delete(testimonialsId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * CHANGE STATUS OF testimonials
	 **************/
	@CrossOrigin
	@GetMapping("/change-status/{testimonialsId}")
	public Response changeStatus(@PathVariable("testimonialsId") Long testimonialsId) {
		Response response = new Response();
		try {
			return testimonialsService.changeStatus(testimonialsId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * CHANGE STATUS OF SLIDER
	 **************/
	@CrossOrigin
	@GetMapping("/list")
	public Response list() {
		Response response = new Response();
		try {
			return testimonialsService.list();
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

}
