/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Ishwar Bathe
 * @since Oct 12, 2020
 * 
 */

@Entity
@Table(name = "app_setting")
public class AppSetting implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "app_setting_id")
	private Long appSettingId;
	
	@Column(name = "appkey")
	private String appKey;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "type")
	private String type;

	@Column(name = "description", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String description;

	public Long getAppSettingId() {
		return appSettingId;
	}

	public void setAppSettingId(Long appSettingId) {
		this.appSettingId = appSettingId;
	}



	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AppSetting [appSettingId=" + appSettingId + ", appKey=" + appKey + ", value=" + value + ", type=" + type
				+ ", description=" + description + "]";
	}
	
	
	
	
	
}
