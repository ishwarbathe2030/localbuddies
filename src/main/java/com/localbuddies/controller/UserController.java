/**
 * 
 */
package com.localbuddies.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.cc.UserCC;
import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.UserService;


/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/user")
public class UserController {


	private final UserService userService;
	
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	
	/****
	 * UPDATE PROFILE
	 **************/
	@CrossOrigin
	@PutMapping("/update_profile_pic")
	public Response updateProfile(@RequestBody UserCC userCC) {
		Response response = new Response();
		try {
			return response;
//			return userService.adminUserList(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}


	/****
	 * ALL USER LIST FOR ADMIN PANEL
	 **************/
	@CrossOrigin
	@PostMapping("/admin-user-list")
	public Response adminUserList(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return userService.adminUserList(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * USER REPORTS
	 **************/
	@CrossOrigin
	@PostMapping("/admin-user-report")
	public Response userReports(@RequestBody Filter filter) {
		Response response = new Response();
		try {
			return userService.userReports(filter);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
}
