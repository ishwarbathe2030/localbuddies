/**
 * 
 */
package com.localbuddies.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbuddies.bean.LogErrorFile;
import com.localbuddies.cc.Authentication;
import com.localbuddies.constants.AuthenticationException;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.service.AuthenticationService;
import com.localbuddies.setting.RepositoryDao;

/**
 * @author Ishwar Bathe
 * @since Aug 27, 2020
 * 
 */

@RestController
@RequestMapping(value = "/v1/authentication")
public class AuthenticationController {
	
	private static final Logger log = LogManager.getLogger(AuthenticationController.class);

	private final AuthenticationService authenticationService;
	private final RepositoryDao repositoryDao;

	public AuthenticationController(AuthenticationService authenticationService,RepositoryDao repositoryDao) {
		this.authenticationService = authenticationService;
		this.repositoryDao=repositoryDao;
	}

	/****
	 * REGISTRATION OF NEW USER
	 * @throws Exception 
	 * @throws Throwable 
	 **************/

	@CrossOrigin
	@PostMapping("/sign-up")
	public Response addnew(@RequestBody Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			return authenticationService.signUp(authentication);
		}catch (AuthenticationException tu) {
			response.setStatus(tu.getErrorCode());
			response.setTitle(tu.getTitle());
			response.setMessage(tu.getMessage());
			return response;
		}catch (Exception e) {
			LogErrorFile errorFile=new LogErrorFile();
			errorFile.setErrorClass(e.getClass().toString());
			errorFile.setErrorText(repositoryDao.ErrorText(e));
			errorFile.setMethodName("sign up");
			errorFile.setClassName(log.getName());
			repositoryDao.addNewId(errorFile);
			
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * Login With Password
	 **************/

	@CrossOrigin
	@PostMapping("/login-with-password")
	public Response loginWithPass(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.loginWithPass(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * Login With Password
	 **************/

	@CrossOrigin
	@PostMapping("/login-with-otp")
	public Response loginWithOTP(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.loginWithOTP(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	/****
	 * Change Password
	 **************/

	@CrossOrigin
	@PostMapping("/change-password")
	public Response changePassowrd(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.changePassword(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	

	/****
	 * Update User Profile
	 **************/

	@CrossOrigin
	@PutMapping("/update-profile")
	public Response updateUser(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.updateUser(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * Get User Account
	 **************/

	@CrossOrigin
	@PostMapping("/find-user-account")
	public Response findUser(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.findUser(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	
	/****
	 * SENT OTP
	 **************/

	@CrossOrigin
	@PostMapping("/send-otp")
	public Response sendOTP(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.sendOTP(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * VERIFY OTP
	 **************/

	@CrossOrigin
	@PostMapping("/verify-otp")
	public Response verifyOTP(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.verifyOTP(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	/****
	 * CHANGE PASSWORD AFTER VERIFING OTP
	 **************/

	@CrossOrigin
	@PostMapping("/set-new-password")
	public Response changePasswordByOtp(@RequestBody Authentication authentication) {
		Response response = new Response();
		try {
			return authenticationService.changePasswordByOtp(authentication);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}

	
	
	/****
	 * LOGOUT 
	 **************/
	@CrossOrigin
	@GetMapping("/logout/{userId}/{loggerId}/{logoutFromAllDevice}")
	public Response list(@PathVariable("userId") Long userId, @PathVariable("loggerId") Long loggerId,@PathVariable("loggerId") boolean logoutFromAllDevice) {
		Response response = new Response();
		try {
			return authenticationService.logOut(userId,loggerId,logoutFromAllDevice);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
	
	/****
	 * LOGOUT 
	 **************/
	@CrossOrigin
	@GetMapping("/logout-from-all/{userId}")
	public Response list(@PathVariable("userId") Long userId) {
		Response response = new Response();
		try {
			return authenticationService.logOutAll(userId);
		} catch (Exception e) {
			response.setStatus(StatusConstance.SERVER_ERROR);
			response.setMessage(StatusConstance.SERVER_ERROR_MSG);
			return response;
		}
	}
	
	
}
