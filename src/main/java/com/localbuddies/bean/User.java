/**
 * 
 */
package com.localbuddies.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */

@Entity
@Table(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "username", unique = true, nullable = true, length = 50)
	private String username;

	@Column(name = "email", unique = true, length = 255)
	private String email;

	@Column(name = "mobile", unique = true, length = 20)
	private String mobile;

	@Column(name = "first_name", nullable = true, length = 25)
	private String firstName;

	@Column(name = "middle_name", nullable = true, length = 25)
	private String middleName;

	@Column(name = "last_name", nullable = true, length = 25)
	private String lastName;

	@Column(name = "gender", nullable = true, length = 1)
	private Integer gender;

	@Column(name = "dob", nullable = true)
	private Date dob;

	@Column(name = "uuid", unique = true, nullable = false, length = 50)
	private String uuid;

	@Column(name = "profile_photo", nullable = true, length = 25)
	private String profilePhoto;

	@Column(name = "access_token", columnDefinition = "LONGTEXT DEFAULT NULL")
	private String accessToken;

	/***
	 * 1 for Webiste 2 For Android 3 for Ios
	 *********/
	@Column(name = "register_by", nullable = true)
	private Integer registerBy;

	/***
	 * 1 for Manually 2 For Google 3 for Facebook 4 for Instagram
	 *********/
	@Column(name = "register_with", nullable = true)
	private Integer registerWith;

	/***
	 * 0 For In-active 1 For active 2 For Inactive by admin
	 *********/
	@Column(name = "status", nullable = true, columnDefinition = "INTEGER DEFAULT 1")
	private Integer status;

	/***
	 * 1 For Admin 2 For Customer
	 *********/
	@Column(name = "user_type", nullable = true, columnDefinition = "INTEGER DEFAULT 2")
	private Integer userType;

	@Column(name = "login_alert", columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean loginAlert;

	@Column(name = "social_user", nullable = true)
	private Long socialUserId;

	@Column(name = "password_reset_date", nullable = true)
	private String passwordResetDate;

	/***
	 * 1 For Verified 2 For Un-Verified
	 *********/
	@Column(name = "user_verfied", nullable = true, columnDefinition = "INTEGER DEFAULT 1")
	private Integer userVerfied;

	@JsonIgnore
	@Column(name = "password_hash", nullable = true)
	private String passwordHash;

	@JsonIgnore
	@Column(name = "password_code", nullable = true)
	private String passwordCode;

	@Column(name = "user_remember_hash", nullable = true)
	private String userRememberHash;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modify_date")
	private Date modifyDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login_date")
	private Date lastLogging;

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the gender
	 */
	public Integer getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(Integer gender) {
		this.gender = gender;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the profilePhoto
	 */
	public String getProfilePhoto() {
		return profilePhoto;
	}

	/**
	 * @param profilePhoto
	 *            the profilePhoto to set
	 */
	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	/**
	 * @return the registerBy
	 */
	public Integer getRegisterBy() {
		return registerBy;
	}

	/**
	 * @param registerBy
	 *            the registerBy to set
	 */
	public void setRegisterBy(Integer registerBy) {
		this.registerBy = registerBy;
	}

	/**
	 * @return the registerWith
	 */
	public Integer getRegisterWith() {
		return registerWith;
	}

	/**
	 * @param registerWith
	 *            the registerWith to set
	 */
	public void setRegisterWith(Integer registerWith) {
		this.registerWith = registerWith;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * @return the loginAlert
	 */
	public boolean isLoginAlert() {
		return loginAlert;
	}

	/**
	 * @param loginAlert
	 *            the loginAlert to set
	 */
	public void setLoginAlert(boolean loginAlert) {
		this.loginAlert = loginAlert;
	}

	/**
	 * @return the socialUserId
	 */
	public Long getSocialUserId() {
		return socialUserId;
	}

	/**
	 * @param socialUserId
	 *            the socialUserId to set
	 */
	public void setSocialUserId(Long socialUserId) {
		this.socialUserId = socialUserId;
	}

	public String getPasswordResetDate() {
		return passwordResetDate;
	}

	public void setPasswordResetDate(String passwordResetDate) {
		this.passwordResetDate = passwordResetDate;
	}

	/**
	 * @return the userVerfied
	 */
	public Integer getUserVerfied() {
		return userVerfied;
	}

	/**
	 * @param userVerfied
	 *            the userVerfied to set
	 */
	public void setUserVerfied(Integer userVerfied) {
		this.userVerfied = userVerfied;
	}

	/**
	 * @return the passwordHash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash
	 *            the passwordHash to set
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * @return the userRememberHash
	 */
	public String getUserRememberHash() {
		return userRememberHash;
	}

	/**
	 * @param userRememberHash
	 *            the userRememberHash to set
	 */
	public void setUserRememberHash(String userRememberHash) {
		this.userRememberHash = userRememberHash;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate
	 *            the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid
	 *            the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPasswordCode() {
		return passwordCode;
	}

	public void setPasswordCode(String passwordCode) {
		this.passwordCode = passwordCode;
	}

	public Date getLastLogging() {
		return lastLogging;
	}

	public void setLastLogging(Date lastLogging) {
		this.lastLogging = lastLogging;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
