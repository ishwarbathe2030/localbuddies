/**
 * 
 */
package com.localbuddies.service;

import com.localbuddies.cc.Authentication;
import com.localbuddies.constants.Response;

/**
 * @author Ishwar Bathe
 * @since Aug 27, 2020
 * 
 */
public interface AuthenticationService {

	public Response loginWithPass(Authentication authentication) throws Exception;

	public Response signUp(Authentication authentication) throws Exception;

	public Response findUser(Authentication authentication) throws Exception;

	public Response sendOTP(Authentication authentication) throws Exception;

	public Response verifyOTP(Authentication authentication) throws Exception;

	public Response loginWithOTP(Authentication authentication) throws Exception;

	public Response changePassword(Authentication authentication) throws Exception;
	
	public Response changePasswordByOtp(Authentication authentication) throws Exception;

	public Response uploadProfilePhoto(Authentication authentication) throws Exception;

	public Response addNewUser(Authentication authentication) throws Exception;

	public Response updateUser(Authentication authentication) throws Exception;

	public Response changeUserStatus(Authentication authentication) throws Exception;
	
	public Response loggedDevices(Long userId) throws Exception;
	
	public Response logOut(Long userId, Long loggerId,boolean logoutFromAllDevice) throws Exception;
	
	public Response logOutAll(Long userId) throws Exception;

}
