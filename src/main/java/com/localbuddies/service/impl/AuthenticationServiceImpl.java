/**
 * 
 */
package com.localbuddies.service.impl;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.bean.User;
import com.localbuddies.bean.UserLogger;
import com.localbuddies.bean.UserOtpAuth;
import com.localbuddies.bean.UserPastPassword;
import com.localbuddies.cc.Authentication;
import com.localbuddies.constants.AuthenticationException;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.UserDao;
import com.localbuddies.service.AuthenticationService;
import com.localbuddies.setting.RepositoryDao;
import com.localbuddies.util.RandomGenerator;
import com.localbuddies.util.SecurityEncorder;
import com.localbuddies.util.SendMail;

/**
 * @author Ishwar Bathe
 * @since Aug 27, 2020
 * 
 */

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private final RepositoryDao repositoryDao;

	private final SendMail sendMail;

	private final UserDao userDao;

	private static final SecureRandom secureRandom = new SecureRandom(); // threadsafe

	private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); // threadsafe

	DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

	public AuthenticationServiceImpl(RepositoryDao repositoryDao, SendMail sendMail, UserDao userDao) {
		super();
		this.repositoryDao = repositoryDao;
		this.sendMail = sendMail;
		this.userDao = userDao;
	}

	public static String generateNewToken() {
		byte[] randomBytes = new byte[100];
		secureRandom.nextBytes(randomBytes);
		return base64Encoder.encodeToString(randomBytes);
	}

	@Override
	public Response loginWithPass(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

			User user = null;
			if (authentication.getMobile() != null) {
				user = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}
			if (authentication.getEmail() != null) {
				user = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}
			if (user != null) {

				// CHeck if user logged to other device
				List<UserLogger> loggerList = userDao.loggedDevices(user.getUserId());
				int currentlyLoggingDevices = 0;
				Long lastLoggedId = 0L;
				for (UserLogger logger : loggerList) {
					if (logger.isLogged()) {
						currentlyLoggingDevices++;
						lastLoggedId = logger.getLoggerId();
					}
				}

				if (currentlyLoggingDevices >= 1 && user.getUserType() == 2) {
					if (lastLoggedId > 0) {
						response.setResponse(user.getUserId());
					}
					response.setTitle("ALREADY LOGGED");
					response.setStatus(StatusConstance.ALREADY_LOGGED);
					response.setMessage(
							"By this credentials you are already logged to other device. Want to LOGOUT from other Device? Click Here..");

				} else {
					boolean isPasswordCorrect = SecurityEncorder.comparePassword(authentication.getPassword(),
							user.getPasswordHash(), user.getPasswordCode());
					repositoryDao.update(user);
					if (isPasswordCorrect) {
						Authentication authenticated = new Authentication();
						authenticated.setUserId(user.getUserId());
						authenticated.setFirstName(user.getFirstName());
						authenticated.setLastName(user.getLastName());
						authenticated.setEmail(user.getEmail());
						authenticated.setMiddleName(user.getMiddleName());
						authenticated.setGender(user.getGender());
						authenticated.setDob(user.getDob());
						authenticated.setProfilePhoto(user.getProfilePhoto());
						authenticated.setRegisterBy(user.getRegisterBy());
						authenticated.setRegisterWith(user.getRegisterWith());
						authenticated.setUsername(user.getUsername());
						authenticated.setMobile(user.getMobile());
						authenticated.setUuid(user.getUuid());
						authenticated.setAuthToken(user.getPasswordCode());
						authenticated.setAccessToken(user.getAccessToken());
						authenticated.setUserType(user.getUserType());

						UserLogger logger = new UserLogger();
						logger.setDeviceUuid("UDUI" + RandomGenerator.generateRandamNo());
						logger.setFcm(authentication.getFcm());
						logger.setLatitudes(authentication.getLatitudes());
						logger.setLoggedCity(authentication.getLoggedCity());
						logger.setLongitudes(authentication.getLongitudes());
						logger.setManufacturer(authentication.getManufacturer());
						logger.setModel(authentication.getModel());
						logger.setPlatform(authentication.getPlatform());
						logger.setLogged(true);
						logger.setUniqueDeviceId(authentication.getUniqueDeviceId());
						logger.setVersion(authentication.getVersion());
						logger.setUser(user);
						authenticated.setDeviceUuid(logger.getDeviceUuid());
						repositoryDao.addnew(logger);
						authenticated.setLoggerId(logger.getLoggerId());

						response.setResponse(authenticated);
						response.setTitle(StatusConstance.LOGIN_SUCCESSFULL);
						response.setStatus(StatusConstance.SUCCESS);
						response.setMessage(StatusConstance.LOGIN_DONE);
					} else {
						response.setTitle(StatusConstance.WRONG_PASSWORD);
						response.setStatus(StatusConstance.ALREADY_PRESENT);
						response.setMessage(StatusConstance.WRONG_PASSWORD_MSG);
					}
				}
			} else {
				response.setTitle(StatusConstance.ACCOUNT_NOT_FOUND_TITLE);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.ACCOUNT_NOT_FOUND);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response loginWithOTP(Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			User user = null;
			if (authentication.getMobile() != null) {
				user = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}
			if (authentication.getEmail() != null) {
				user = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}
			if (user != null) {
				if (authentication.getUuid() != null && !authentication.getUuid().isEmpty()) {
					UserOtpAuth otpAuth = repositoryDao.findBySingleKey(UserOtpAuth.class, "uuid",
							authentication.getUuid());
					if (otpAuth != null) {
						if (otpAuth.getOtp().equals(authentication.getOtp())) {

							Authentication authenticated = new Authentication();
							authenticated.setUserId(user.getUserId());
							authenticated.setFirstName(user.getFirstName());
							authenticated.setLastName(user.getLastName());
							authenticated.setEmail(user.getEmail());
							authenticated.setMiddleName(user.getMiddleName());
							authenticated.setGender(user.getGender());
							authenticated.setDob(user.getDob());
							authenticated.setProfilePhoto(user.getProfilePhoto());
							authenticated.setRegisterBy(user.getRegisterBy());
							authenticated.setRegisterWith(user.getRegisterWith());
							authenticated.setUsername(user.getUsername());
							authenticated.setMobile(user.getMobile());
							authenticated.setUuid(user.getUuid());
							authenticated.setAuthToken(user.getPasswordCode());
							authenticated.setAccessToken(user.getAccessToken());
							authenticated.setUserType(user.getUserType());

							UserLogger logger = new UserLogger();
							logger.setDeviceUuid("UDUI" + RandomGenerator.generateRandamNo());
							logger.setFcm(authentication.getFcm());
							logger.setLatitudes(authentication.getLatitudes());
							logger.setLoggedCity(authentication.getLoggedCity());
							logger.setLongitudes(authentication.getLongitudes());
							logger.setManufacturer(authentication.getManufacturer());
							logger.setModel(authentication.getModel());
							logger.setPlatform(authentication.getPlatform());
							logger.setLogged(true);
							logger.setUniqueDeviceId(authentication.getUniqueDeviceId());
							logger.setVersion(authentication.getVersion());
							logger.setUser(user);
							authenticated.setDeviceUuid(logger.getDeviceUuid());
							repositoryDao.addnew(logger);
							authenticated.setLoggerId(logger.getLoggerId());

							response.setResponse(authenticated);
							response.setTitle(StatusConstance.LOGIN_SUCCESSFULL);
							response.setStatus(StatusConstance.SUCCESS);
							response.setMessage(StatusConstance.LOGIN_DONE);

						} else {
							response.setTitle("INVALID OTP");
							response.setStatus(StatusConstance.ALREADY_PRESENT);
							response.setMessage("Invalid OTP. Please try again");
						}

					} else {
						response.setTitle("OTP NOT REQUESTED");
						response.setStatus(StatusConstance.NOT_FOUND);
						response.setMessage("You are not requested for otp.");
					}
				} else {
					response.setTitle("UUID REQUIRED");
					response.setStatus(StatusConstance.MANDATORY);
					response.setMessage("User Unique id is required for vefiry otp");
				}

			} else {
				response.setTitle(StatusConstance.ACCOUNT_NOT_FOUND_TITLE);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.ACCOUNT_NOT_FOUND);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Response signUp(Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			User userMobile = null;
			User userEmail = null;

			if (authentication.getMobile() != null) {
				userMobile = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}
			if (authentication.getEmail() != null) {
				userEmail = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}

			if (userMobile != null) {
				throw new AuthenticationException(StatusConstance.ALREADY_PRESENT, StatusConstance.ALREADY_PRESENT_TITLE, StatusConstance.ALREADY_PRESENT_MOBILE_MSG);
			}
			if (userEmail != null) {
				throw new AuthenticationException(StatusConstance.ALREADY_PRESENT, StatusConstance.ALREADY_PRESENT_TITLE, StatusConstance.ALREADY_PRESENT_EMAIL_MSG);
				/*
				 * response.setTitle(StatusConstance.ALREADY_PRESENT_TITLE);
				 * response.setStatus(StatusConstance.ALREADY_PRESENT_EMAIL);
				 * response.setMessage(StatusConstance.ALREADY_PRESENT_EMAIL_MSG);
				 */
			}
			if (userMobile == null && userEmail == null) {
				if (authentication.getPassword() != null) {
					User user = new User();
					user.setFirstName(authentication.getFirstName());
					user.setMiddleName(authentication.getMiddleName());
					user.setLastName(authentication.getLastName());
					user.setEmail(authentication.getEmail());
					user.setMobile(authentication.getMobile());
					String encodedPassword = SecurityEncorder.encode(authentication.getPassword());
					String[] ecodedArray = encodedPassword.split(":");
					user.setPasswordCode(ecodedArray[0]);
					user.setPasswordHash(ecodedArray[1]);
					user.setRegisterBy(authentication.getRegisterBy());
					user.setRegisterWith(1);
					user.setStatus(1);
					user.setUsername(authentication.getUsername());
					user.setUserType(2);
					user.setUserVerfied(1);
					user.setUuid("UUID" + RandomGenerator.generateRandamNo());
					user.setAccessToken(generateNewToken());
					repositoryDao.addNewId(user);

					Authentication authenticated = new Authentication();
					authenticated.setUserId(user.getUserId());
					authenticated.setFirstName(user.getFirstName());
					authenticated.setLastName(user.getLastName());
					authenticated.setEmail(user.getEmail());
					authenticated.setMiddleName(user.getMiddleName());
					authenticated.setGender(user.getGender());
					authenticated.setDob(user.getDob());
					authenticated.setProfilePhoto(user.getProfilePhoto());
					authenticated.setRegisterBy(user.getRegisterBy());
					authenticated.setRegisterWith(user.getRegisterWith());
					authenticated.setUsername(user.getUsername());
					authenticated.setMobile(user.getMobile());
					authenticated.setUuid(user.getUuid());
					authenticated.setAuthToken(user.getPasswordCode());
					authenticated.setAccessToken(user.getAccessToken());
					authenticated.setUserType(user.getUserType());

					System.out.println("***********************************");
					/*
					 * User user1 = null;
					 * 
					 * System.out.println("*************************:" + user1.getEmail());
					 */
					UserLogger logger = new UserLogger();
					logger.setDeviceUuid("UDUI" + RandomGenerator.generateRandamNo());
					logger.setFcm(authentication.getFcm());
					logger.setLatitudes(authentication.getLatitudes());
					logger.setLoggedCity(authentication.getLoggedCity());
					logger.setLongitudes(authentication.getLongitudes());
					logger.setManufacturer(authentication.getManufacturer());
					logger.setModel(authentication.getModel());
					logger.setPlatform(authentication.getPlatform());
					logger.setLogged(true);
					logger.setUniqueDeviceId(authentication.getUniqueDeviceId());
					logger.setVersion(authentication.getVersion());
					logger.setUser(user);
					authenticated.setDeviceUuid(logger.getDeviceUuid());
					repositoryDao.addNewId(logger);
					authenticated.setLoggerId(logger.getLoggerId());

					response.setResponse(authenticated);

					response.setTitle(StatusConstance.REGISTRATION_DONE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.REGISTRATION_SUCCESSFULL);
				} else {
					throw new AuthenticationException(StatusConstance.MANDATORY, StatusConstance.PASSOWORD_REQ_TITILE, StatusConstance.PASSWORD_REQUIRED_MSG);
					/*
					 * response.setTitle(StatusConstance.PASSOWORD_REQ_TITILE);
					 * response.setStatus(StatusConstance.MANDATORY);
					 * response.setMessage(StatusConstance.PASSWORD_REQUIRED_MSG);
					 */
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response findUser(Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			User user = null;
			if (authentication.getMobile() != null) {
				user = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}

			if (authentication.getEmail() != null) {
				user = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}

			if (authentication.getUsername() != null) {
				user = repositoryDao.findBySingleKey(User.class, "username", authentication.getEmail());
			}

			if (authentication.getUuid() != null) {
				user = repositoryDao.findBySingleKey(User.class, "uuid", authentication.getUuid());
			}

			if (authentication.getUserId() != null) {
				user = repositoryDao.findById(User.class, authentication.getUserId());
			}

			if (user != null) {
				Authentication authenticated = new Authentication();
				authenticated.setUserId(user.getUserId());
				authenticated.setFirstName(user.getFirstName());
				authenticated.setLastName(user.getLastName());
				authenticated.setEmail(user.getEmail());
				authenticated.setMiddleName(user.getMiddleName());
				authenticated.setGender(user.getGender());
				authenticated.setDob(user.getDob());
				authenticated.setProfilePhoto(user.getProfilePhoto());
				authenticated.setRegisterBy(user.getRegisterBy());
				authenticated.setRegisterWith(user.getRegisterWith());
				authenticated.setUsername(user.getUsername());
				authenticated.setMobile(user.getMobile());
				authenticated.setUuid(user.getUuid());
				authenticated.setAuthToken(user.getPasswordCode());
				authenticated.setAccessToken(user.getAccessToken());
				response.setResponse(authenticated);
				response.setTitle(StatusConstance.DATA_FOUND);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.RECORD_FOUND);
			} else {
				response.setTitle(StatusConstance.USER_NOT_FOUND);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage("Account not found. Please check mobile no.");
			}

		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response sendOTP(Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			// AppSetting appsetting =
			// repositoryDao.findBySingleKey(AppSetting.class, "appKey",
			// "APP_NAME");

			if (authentication.getMobile() == null && authentication.getEmail() == null) {
				response.setTitle("MOBILE OR EMAIL REQUIRED");
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("To send otp mobile no or email is required");

			} else {
				UserOtpAuth otpAuth = repositoryDao.findBySingleKey(UserOtpAuth.class, "uuid",
						authentication.getUuid());
				if (otpAuth != null) {
					repositoryDao.update(otpAuth);
					List<String> uuid = new ArrayList<String>();
					uuid.add(otpAuth.getUuid());
					response.setResponse(uuid);

					// SENT OTP ON MOBILE
					if (authentication.getMobile() != null) {
						// String otpMsg = otpAuth.getOtp() + "is your " +
						// appsetting.getValue() + " "
						// + authentication.getOtpType() + " code. \n" + "NEVER
						// SHARE YOUR OTP WITH ANYONE. "
						// + appsetting.getValue() + " never call or message to
						// ask for the OTP.";
					}

					if (authentication.getEmail() != null) {
						sendOrderMail(authentication.getEmail(), otpAuth.getOtp(), authentication.getOtpType());
					}

					response.setTitle("OTP RESEND SUCCESSFULLY");
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage("Otp resend successfully");
				} else {
					otpAuth = new UserOtpAuth();
					otpAuth.setOtp(RandomGenerator.generateNum(6));
					otpAuth.setUuid("AUTH" + RandomGenerator.generateNum(8));
					repositoryDao.addnew(otpAuth);

					// SENT OTP ON MOBILE
					if (authentication.getMobile() != null) {
						// String otpMsg = otpAuth.getOtp() + " is your " +
						// appsetting.getValue() + " "
						// + authentication.getOtpType() + " code. \n" + "NEVER
						// SHARE YOUR OTP WITH ANYONE. "
						// + appsetting.getValue() + " never call or message to
						// ask for the OTP.";
					}

					if (authentication.getEmail() != null) {
						sendOrderMail(authentication.getEmail(), otpAuth.getOtp(), authentication.getOtpType());
					}

					List<String> uuid = new ArrayList<String>();
					uuid.add(otpAuth.getUuid());
					response.setResponse(uuid);
					response.setTitle("OTP SEND SUCCESSFULLY");
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage("Otp send successfully");
				}
			}

		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	// SEND OTP ON MAIL
	public void sendOrderMail(String email, String otp, String otpType) throws Exception {
		AppSetting appsetting = repositoryDao.findBySingleKey(AppSetting.class, "appKey", "APP_NAME");
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setOrder(0);

		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);

		Context context = new Context();
		context.setVariable("otp", otp);
		context.setVariable("appname", appsetting.getValue());
		String body = templateEngine.process("new-order.html", context);

		String otpType1 = otpType.substring(0, 1).toUpperCase() + otpType.substring(1);
		sendMail.sendEmail(email, otpType1, body);
	}

	@Override
	public Response verifyOTP(Authentication authentication) throws Exception {
		Response response = new Response();
		try {
			if (authentication.getUuid() != null && !authentication.getUuid().isEmpty()) {
				UserOtpAuth otpAuth = repositoryDao.findBySingleKey(UserOtpAuth.class, "uuid",
						authentication.getUuid());
				if (otpAuth != null) {
					if (otpAuth.getOtp().equals(authentication.getOtp())) {
						response.setTitle("OTP VERIFIED");
						response.setStatus(StatusConstance.SUCCESS);
						response.setMessage("OTP Verified Successfully");
					} else {
						response.setTitle("INVALID OTP");
						response.setStatus(StatusConstance.ALREADY_PRESENT);
						response.setMessage("Invalid OTP. Please try again");
					}

				} else {
					response.setTitle("OTP NOT REQUESTED");
					response.setStatus(StatusConstance.NOT_FOUND);
					response.setMessage("You are not requested for otp.");
				}
			} else {
				response.setTitle("UUID REQUIRED");
				response.setStatus(StatusConstance.MANDATORY);
				response.setMessage("User Unique id is required for vefiry otp");
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changePassword(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

			User user = null;
			if (authentication.getMobile() != null) {
				user = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}

			if (authentication.getEmail() != null) {
				user = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}

			if (authentication.getUsername() != null) {
				user = repositoryDao.findBySingleKey(User.class, "username", authentication.getEmail());
			}

			if (authentication.getUuid() != null) {
				user = repositoryDao.findBySingleKey(User.class, "uuid", authentication.getUuid());
			}

			if (authentication.getUserId() != null) {
				user = repositoryDao.findById(User.class, authentication.getUserId());
			}
			if (user != null) {

				boolean isPasswordCorrect = SecurityEncorder.comparePassword(authentication.getOldPassword(),
						user.getPasswordHash(), user.getPasswordCode());
				if (isPasswordCorrect) {
					UserPastPassword pastPass = new UserPastPassword();
					pastPass.setPasswordCode(user.getPasswordCode());
					pastPass.setPasswordHash(user.getPasswordHash());
					pastPass.setUser(user);
					repositoryDao.addnew(pastPass);

					String encodedPassword = SecurityEncorder.encode(authentication.getPassword());
					String[] ecodedArray = encodedPassword.split(":");
					user.setPasswordCode(ecodedArray[0]);
					user.setPasswordHash(ecodedArray[1]);

					user.setPasswordResetDate(df.format(pastPass.getCreateDate()));
					repositoryDao.update(user);

					response.setTitle(StatusConstance.CHANGE_PASS_TITLE);
					response.setStatus(StatusConstance.SUCCESS);
					response.setMessage(StatusConstance.CHANGE_PASS_MSG);

				} else {
					response.setTitle(StatusConstance.OLD_PASSWORD);
					response.setStatus(StatusConstance.ALREADY_PRESENT);
					response.setMessage(StatusConstance.OLD_PASSWORD_MSG);
				}
			} else {
				response.setTitle(StatusConstance.USER_NOT_FOUND);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.USER_NOT_FOUND_MSG);
			}
		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changePasswordByOtp(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

			User user = null;
			if (authentication.getMobile() != null) {
				user = repositoryDao.findBySingleKey(User.class, "mobile", authentication.getMobile());
			}

			if (authentication.getEmail() != null) {
				user = repositoryDao.findBySingleKey(User.class, "email", authentication.getEmail());
			}

			if (authentication.getUsername() != null) {
				user = repositoryDao.findBySingleKey(User.class, "username", authentication.getEmail());
			}

			if (authentication.getUserId() != null && authentication.getUserId() > 0) {
				user = repositoryDao.findById(User.class, authentication.getUserId());
			}

			if (user != null) {

				UserPastPassword pastPass = new UserPastPassword();
				pastPass.setPasswordCode(user.getPasswordCode());
				pastPass.setPasswordHash(user.getPasswordHash());
				pastPass.setUser(user);
				repositoryDao.addnew(pastPass);

				String encodedPassword = SecurityEncorder.encode(authentication.getPassword());
				String[] ecodedArray = encodedPassword.split(":");
				user.setPasswordCode(ecodedArray[0]);
				user.setPasswordHash(ecodedArray[1]);
				user.setPasswordResetDate(df.format(pastPass.getCreateDate()));

				repositoryDao.update(user);
				response.setTitle(StatusConstance.CHANGE_PASS_TITLE);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.CHANGE_PASS_MSG);

			} else {
				response.setTitle(StatusConstance.USER_NOT_FOUND);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.USER_NOT_FOUND_MSG);
			}
		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response uploadProfilePhoto(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response addNewUser(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response updateUser(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

			User user = repositoryDao.findById(User.class, authentication.getUserId());
			if (user != null) {
				user.setUserId(authentication.getUserId());
				user.setFirstName(authentication.getFirstName());
				user.setLastName(authentication.getLastName());
				user.setEmail(authentication.getEmail());
				user.setMiddleName(authentication.getMiddleName());
				user.setGender(authentication.getGender());
				user.setDob(authentication.getDob());
				user.setProfilePhoto(authentication.getProfilePhoto());
				user.setRegisterBy(authentication.getRegisterBy());
				user.setRegisterWith(authentication.getRegisterWith());
				user.setUsername(authentication.getUsername());
				user.setMobile(authentication.getMobile());
				user.setUuid(authentication.getUuid());
				repositoryDao.update(user);
				response.setResponse(user);
				response.setTitle(StatusConstance.PROFILE_UPDATED_TITLE);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.PROFILE_UPDATED_MSG);
			} else {
				response.setTitle(StatusConstance.USER_NOT_FOUND);
				response.setStatus(StatusConstance.NOT_FOUND);
				response.setMessage(StatusConstance.ACCOUNT_NOT_FOUND);
			}
		} catch (

		Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changeUserStatus(Authentication authentication) throws Exception {
		Response response = new Response();
		try {

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response loggedDevices(Long userId) throws Exception {
		Response response = new Response();
		try {
			response.setResponse(userDao.loggedDevices(userId));
			response.setTitle("DELIVCE LIST");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("User all past logged device list");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response logOut(Long userId, Long loggerId, boolean logoutFromAllDevice) throws Exception {
		Response response = new Response();
		try {
			if (logoutFromAllDevice) {
				List<UserLogger> loggerList = userDao.loggedDevices(userId);
				UserLogger logged = null;
				for (UserLogger logger : loggerList) {
					logged = repositoryDao.findById(UserLogger.class, logger.getLoggerId());
					logged.setLogged(false);
					repositoryDao.update(logged);
				}
			} else {
				UserLogger logger = repositoryDao.findById(UserLogger.class, loggerId);
				logger.setLogged(false);
				repositoryDao.update(logger);
			}

			response.setStatus(200);
			response.setMessage("Logout Successfully");

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response logOutAll(Long userId) throws Exception {
		Response response = new Response();
		try {
			List<UserLogger> loggerList = userDao.loggedDevices(userId);
			UserLogger logged = null;
			for (UserLogger logger : loggerList) {
				logged = repositoryDao.findById(UserLogger.class, logger.getLoggerId());
				logged.setLogged(false);
				repositoryDao.update(logged);
			}
			response.setStatus(200);
			response.setMessage("Logout Successfully From All Devices.");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
