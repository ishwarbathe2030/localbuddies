/**
 * 
 */
package com.localbuddies.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.localbuddies.bean.Slider;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.SliderDAO;
import com.localbuddies.service.SliderServices;
import com.localbuddies.setting.RepositoryDao;
import com.localbuddies.util.SaveFile;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */

@Service
public class SliderServicesImpl implements SliderServices {

	private final RepositoryDao repositoryDao;

	private final SliderDAO sliderDAO;

	private final SaveFile saveFile;

	public SliderServicesImpl(RepositoryDao repositoryDao, SliderDAO sliderDAO, SaveFile saveFile) {
		super();
		this.repositoryDao = repositoryDao;
		this.sliderDAO = sliderDAO;
		this.saveFile = saveFile;
	}

	@Override
	public Response addnew(Slider slider) throws Exception {
		Response response = new Response();
		try {
			if (null != slider.getBase64() && !slider.getBase64().isEmpty()) {
				slider.setImage(saveFile.saveBase64(slider.getBase64(), slider.getImageName()));
			}
			List<Slider> sliderImages = sliderDAO.list();
			if (null != sliderImages && sliderImages.size() > 0) {
				Slider sliderImageDB = sliderImages.get(0);
				Long imageSeq = sliderImageDB.getSeqNo() + 1;
				slider.setSeqNo(imageSeq);
			} else {
				slider.setSeqNo(1l);
			}
			repositoryDao.addnew(slider);
			response.setTitle(StatusConstance.SUCCESS_ADD);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.RECORD_ADDED);
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response delete(Long sliderId) throws Exception {
		Response response = new Response();
		try {
			Slider slider = repositoryDao.findById(Slider.class, sliderId);
			if (null != slider) {
				repositoryDao.deleteObj(slider);
				response.setTitle(StatusConstance.RECORD_DELETED);
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.RECORD_DELETED_MSG);
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response changeStatus(Long sliderId) throws Exception {
		Response response = new Response();
		try {
			Slider slider = repositoryDao.findById(Slider.class, sliderId);
			if (null != slider) {
				if (slider.isStatus())
					slider.setStatus(false);
				else
					slider.setStatus(true);
				repositoryDao.update(slider);
				response.setTitle("SLIDER IMAGE STATUS CHANGED");
				response.setStatus(StatusConstance.SUCCESS);
				response.setMessage(StatusConstance.STATUS_CHANGE_MSG + slider.isStatus());
			}
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response list() throws Exception {
		Response response = new Response();
		try {
			response.setTitle(StatusConstance.LIST_DATA_TITLE);
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage(StatusConstance.LIST_DATA);
			response.setResponse(sliderDAO.list());
		} catch (Exception e) {
			throw e;
		}
		return response;
	}

	@Override
	public Response updatePosition(List<Slider> slider) throws Exception {
		Response response = new Response();
		List<Slider> images = new ArrayList<Slider>();
		try {
			if (null != slider && slider.size() > 0) {
				for (Slider sliderImage : slider) {
					Slider sliderImageDB = repositoryDao.findById(Slider.class, sliderImage.getSliderId());
					sliderImageDB.setSeqNo(sliderImage.getSeqNo());
					repositoryDao.update(sliderImageDB);
					images.add(sliderImageDB);
				}
			}
			response.setResponse(images);
			response.setTitle("NEW SEQUENCE SET");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("Slider New Sequence Set Successfully");

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
