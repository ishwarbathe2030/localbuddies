/**
 * 
 */
package com.localbuddies.service.impl;



import org.springframework.stereotype.Service;

import com.localbuddies.constants.Filter;
import com.localbuddies.constants.Response;
import com.localbuddies.constants.StatusConstance;
import com.localbuddies.dao.UserDao;
import com.localbuddies.service.UserService;

/**
 * @author Ishwar Bathe
 * @since Jul 26, 2020
 * 
 */

@Service
public class UserServiceImpl implements UserService {


	private final UserDao userDao;

	public UserServiceImpl( UserDao userDao) {
		super();
		this.userDao = userDao;
	}


	@Override
	public Response adminUserList(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setResponse(userDao.adminUserList(filter));
			response.setCount(userDao.userListCount(filter));
			response.setTitle("ALL USER LIST");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("All User List");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}


	@Override
	public Response userReports(Filter filter) throws Exception {
		Response response = new Response();
		try {
			response.setResponse(userDao.userReports(filter));
			response.setCount(userDao.userReportsListCount(filter));
			response.setTitle("ALL USER LIST");
			response.setStatus(StatusConstance.SUCCESS);
			response.setMessage("All User List");
		} catch (Exception e) {
			throw e;
		}
		return response;
	}


}
