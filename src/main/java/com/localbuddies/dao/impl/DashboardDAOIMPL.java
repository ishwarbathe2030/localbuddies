/**
 * 
 */
package com.localbuddies.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.localbuddies.cc.Dashboard;
import com.localbuddies.constants.Filter;
import com.localbuddies.dao.DashboardDAO;
import com.localbuddies.setting.ConnectionDao;

/**
 * @author Ishwar Bathe
 * @since Nov 12, 2020
 * 
 */

@Repository(value = "dashboardDAO")
@Transactional
public class DashboardDAOIMPL extends ConnectionDao implements DashboardDAO {

	SimpleDateFormat dtf2 = new SimpleDateFormat("MMM dd", Locale.ENGLISH);

	@Override
	public Dashboard dashboardAnylysis() throws Exception {

		Dashboard dashbaord = null;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT COUNT(o.order_id) as totalOrders, "
					+ " (SELECT ROUND(SUM(ot.total_price),0)as todaysRevenus  FROM orders ot WHERE ot.order_status = 4 ) as totalRevenus,"
					+ " (SELECT COUNT(ot.order_id) as todaysOrders FROM orders ot WHERE DATE(ot.create_date) = CURDATE()) as todaysOrders,"
					+ " (SELECT ROUND(SUM(ot.total_price),0)as todaysRevenus  FROM orders ot WHERE ot.order_status = 4 "
					+ "  AND DATE(ot.create_date) = CURDATE()) as todaysRevenus FROM orders o";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dashbaord = new Dashboard();
				dashbaord.setTodaysOrders(rs.getInt("todaysOrders"));
				dashbaord.setTodaysRevenues(rs.getInt("todaysRevenus"));
				dashbaord.setTotalOrders(rs.getInt("totalOrders"));
				dashbaord.setTotalRevenue(rs.getInt("totalRevenus"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return dashbaord;
	}

	public Dashboard orderAnylysisByStatus(Filter filter) throws Exception {
		List<Integer> orderStatusCount = new ArrayList<Integer>();
		Dashboard dashbaord = new Dashboard();
		;
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT COUNT(o.order_id) AS TOTAL, "
					+ " (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 1";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";

			sql += ") AS SUBMITTED, ";
			sql += " (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 2";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";
			sql += " ) AS ACCEPTTED, ";
			sql += " (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 3";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";
			sql += " ) AS CANCELLED, ";
			sql += " (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 4";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";
			sql += " ) AS DELIVERED, ";
			sql += " (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 5";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";
			sql += ") AS ONROUTE, ";
			sql += "  (SELECT COUNT(o.order_id) FROM orders o WHERE o.order_id IS NOT NULL AND o.order_status = 6";
			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";
			sql += " ) AS FAILED ";
			sql += " FROM orders o WHERE o.order_id IS NOT NULL";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				orderStatusCount.add(rs.getInt("SUBMITTED"));
				orderStatusCount.add(rs.getInt("ACCEPTTED"));
				orderStatusCount.add(rs.getInt("CANCELLED"));
				orderStatusCount.add(rs.getInt("DELIVERED"));
				orderStatusCount.add(rs.getInt("ONROUTE"));
				orderStatusCount.add(rs.getInt("FAILED"));
			}

			List<String> orderStatus = new ArrayList<String>();
			orderStatus.add("SUBMITTED");
			orderStatus.add("ACCEPTTED");
			orderStatus.add("CANCELLED");
			orderStatus.add("DELIVERED");
			orderStatus.add("ONROUTE");
			orderStatus.add("FAILED");

			dashbaord.setOrderStatus(orderStatus);
			dashbaord.setOrderStatusCount(orderStatusCount);
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return dashbaord;
	}

	@Override
	public Dashboard dayWiseOrder(Filter filter) throws Exception {
		List<String> label = new ArrayList<String>();
		List<Integer> data = new ArrayList<Integer>();
		Dashboard dashbaord = new Dashboard();
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT COUNT(*) as count, o.create_date  as dates   FROM orders o  WHERE o.order_id IS NOT NULL ";

			if (filter.getFromDate() != null && !filter.getFromDate().isEmpty())
				sql += " AND DATE(o.create_date) >= DATE('" + filter.getFromDate() + "') ";

			if (filter.getToDate() != null && !filter.getToDate().isEmpty())
				sql += " AND DATE(o.create_date) <= DATE('" + filter.getToDate() + "') ";

			sql += "GROUP BY o.create_date LIMIT 1,10";
			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				label.add(rs.getDate("dates").toString());
				data.add(rs.getInt("count"));
			}
			dashbaord.setData(data);
			dashbaord.setLable(label);
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return dashbaord;
	}

	@Override
	public Dashboard monthWiseOrder(Filter filter) throws Exception {
		List<String> label = new ArrayList<String>();
		List<Integer> data = new ArrayList<Integer>();
		Dashboard dashbaord = new Dashboard();
		Connection conn = null;
		try {
			conn = getDataSource().getConnection();
			String sql = " SELECT COUNT(*) as count , MONTH(o.create_date)  as dates FROM   orders o WHERE  YEAR(o.create_date) = "
					+ filter.getSearchBy();
			sql += " GROUP BY  MONTH(o.create_date)";
			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				label.add(rs.getString("dates"));
				data.add(rs.getInt("count"));
			}
			dashbaord.setData(data);
			dashbaord.setLable(label);
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != conn) {
				conn.close();
			}
		}
		return dashbaord;
	}

}
