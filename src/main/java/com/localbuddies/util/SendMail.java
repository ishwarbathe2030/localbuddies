/**
 * 
 */
package com.localbuddies.util;

import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.localbuddies.bean.AppSetting;
import com.localbuddies.dao.AppSettingDAO;

/**
 * @author Ishwar Bathe
 * @since Sep 6, 2020
 * 
 */

@Component
public class SendMail {

	@Autowired
	private AppSettingDAO appSettingDAO;

	public void sendEmail(String recipientMail, String subject, String body) throws Exception {
		System.out.println("IN SINGLE");
		String host = null;
		int port = 0;
		String sender = null;
		String senderPassword = null;

		List<AppSetting> settingList = appSettingDAO.list("appEmail");
		if (settingList.size() > 0) {

			for (AppSetting setting : settingList) {
				if (setting.getType().equals("Host")) {
					host = setting.getValue();
				}
				if (setting.getType().equals("Port")) {
					port = Integer.parseInt(setting.getValue());
				}
				if (setting.getType().equals("Email")) {
					sender = setting.getValue();
				}
				if (setting.getType().equals("Password")) {
					senderPassword = setting.getValue();
				}
			}
			
			System.out.println("HOST="+host);
			System.out.println("Port="+port);
			System.out.println("sender="+sender);
			System.out.println("senderPassword="+senderPassword);

			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", "smtp.live.com");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");

			Session session = Session.getDefaultInstance(props);
			session.setDebug(true);

			try {
				MimeMessage message = new MimeMessage(session);

				message.setFrom(new InternetAddress(sender));

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientMail));

				message.setSubject(subject);

				MimeMultipart multipart = new MimeMultipart();
				BodyPart messageBodyPart = new MimeBodyPart();

				messageBodyPart.setContent(body, "text/html");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				// message.setText(body);

				Transport trans = session.getTransport("smtp");
				trans.connect(host, port, sender, senderPassword);
				trans.sendMessage(message, message.getAllRecipients());

			} catch (MessagingException mex) {
				mex.printStackTrace();
			}
		}

	}
	
	
	
	public void sendMultiEmail(String[] recipientMail, String subject, String body) throws Exception {
    System.out.println("IN MULTI");
		String host = null;
		int port = 0;
		String sender = null;
		String senderPassword = null;

		List<AppSetting> settingList = appSettingDAO.list("appEmail");
		if (settingList.size() > 0) {

			for (AppSetting setting : settingList) {
				if (setting.getType().equals("Host")) {
					host = setting.getValue();
				}
				if (setting.getType().equals("Port")) {
					port = Integer.parseInt(setting.getValue());
				}
				if (setting.getType().equals("Email")) {
					sender = setting.getValue();
				}
				if (setting.getType().equals("Password")) {
					senderPassword = setting.getValue();
				}
			}
			
			System.out.println("HOST="+host);
			System.out.println("Port="+port);
			System.out.println("sender="+sender);
			System.out.println("senderPassword="+senderPassword);

			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", "smtp.live.com");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");

			Session session = Session.getDefaultInstance(props);
			session.setDebug(true);

			try {
				MimeMessage message = new MimeMessage(session);
				
				message.setFrom(new InternetAddress(sender));
				
				for(int i = 0;i<=recipientMail.length; i++){
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(recipientMail[i]));
				}

				message.setSubject(subject);

				MimeMultipart multipart = new MimeMultipart();
				BodyPart messageBodyPart = new MimeBodyPart();

				messageBodyPart.setContent(body, "text/html");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				// message.setText(body);

				Transport trans = session.getTransport("smtp");
				trans.connect(host, port, sender, senderPassword);
				trans.sendMessage(message, message.getAllRecipients());

			} catch (MessagingException mex) {
				mex.printStackTrace();
			}
		}

	}

	

}
