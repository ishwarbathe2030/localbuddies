/**
 * 
 */
package com.localbuddies.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Ishwar Bathe
 * @since Jul 28, 2020
 * 
 */


@Entity
@Table(name = "slider")
public class Slider implements Serializable {

	private static final long serialVersionUID = -6350362369328216620L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "slider_id")
	private Long sliderId;
	
	@Column(name = "image")
	private String image;
	
	
	@Column(name = "alter_name")
	private String alterName;
	
	
	@Column(name="seq_no")
	private Long seqNo;
	
	@Column(name = "status" , columnDefinition = "BOOLEAN DEFAULT TRUE")
	private boolean status;
	
	
	@Transient
	private String base64;
	
	@Transient
	private String imageName;

	/**
	 * @return the sliderId
	 */
	public Long getSliderId() {
		return sliderId;
	}

	/**
	 * @param sliderId the sliderId to set
	 */
	public void setSliderId(Long sliderId) {
		this.sliderId = sliderId;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the alterName
	 */
	public String getAlterName() {
		return alterName;
	}

	/**
	 * @param alterName the alterName to set
	 */
	public void setAlterName(String alterName) {
		this.alterName = alterName;
	}

	/**
	 * @return the seqNo
	 */
	public Long getSeqNo() {
		return seqNo;
	}

	/**
	 * @param seqNo the seqNo to set
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}


	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the base64
	 */
	public String getBase64() {
		return base64;
	}

	/**
	 * @param base64 the base64 to set
	 */
	public void setBase64(String base64) {
		this.base64 = base64;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	
	
	
	
	
	

}
