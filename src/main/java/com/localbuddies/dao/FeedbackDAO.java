/**
 * 
 */
package com.localbuddies.dao;

import java.util.List;

import com.localbuddies.bean.Feedback;
import com.localbuddies.constants.Filter;


/**
 * @author ISHWAR BATHE
 * @since Oct 21, 2020
 * 
 */
public interface FeedbackDAO  {
	
	List<Feedback> feedbackList(Filter filter)  throws Exception ;

}
