package com.localbuddies.constants;
/**
 * 
 */

/**
 * @author Prashant Kolhe
 * @since Dec 26, 2020
 * 
 */
public class AuthenticationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5472454312463351082L;

	
	private Integer errorCode;
	
	private String title;
	
	public AuthenticationException(Integer errorCode, String title,String errorMessage) {
		super(errorMessage);
		this.title=title;
		this.errorCode = errorCode;
	}
	public Integer getErrorCode() {
		return errorCode;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

}
